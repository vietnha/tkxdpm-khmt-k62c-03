Các use case chọn:

- Trả xe: Trần Đình Thắng
- Thuê xe: Nguyễn Văn Trung
- Quản lý thông tin thẻ: Nguyễn Thị Thu Thành
- Quản lý thông tin bãi xe: Mai Thị Thảo
- Quản lý thông tin xe: Nguyễn Hữu Anh Việt
- Quản lý loại xe: Bùi Phó Bền
- Quản lý giá cho thuê: Nguyễn Thị Duyên

Bài tập nhóm 3: Bùi Phó Bền
Bài tập nhóm 4: Nguyễn Văn Trung
Báo cáo chung:
Mỗi người làm các phần tương ứng với use case của mình.
Mỗi người trong nhóm tự code phần api và client của mình. Sau khi hoàn
thành tự thực hiện ghép mã nguồn trong thư mục Projects.

Phần mã nguồn bao gồm: bankAPI (server bank)
			Assigment 5 (client hệ thống)
			Assigment 5_Server (server hệ thống)

Các thành viên làm các phần công việc sau:
Nguyễn Văn Trung: ccode tất cả BankApi, phần server hệ thống về thông tin thuê xe, xóa xe khỏi 
	bãi, thêm xe vào danh sách xe đang thuê test whitebox/blackbox getRentalData. Tự ghép mã nguồn vào code nhóm
Bùi Phó Bền: code các module liên quan loại xe trong Server, Client, Api: xem loại xe, thêm, sửa, xóa loại xe, tự ghép mã nguồn vào code nhóm
	api lấy thông tin từ id loại xe, api kiểm tra tồn tại loại xe, test whitebox/blackbox update loại xe.
Trần Đình Thắng :
	*Server :  API danh sách xe đang thuê ( bikeBack/rentalDatas) , API trả xe ( bikeBack/id/cardId/parkingId)
	* Client : Giao diện hiện danh sách xe đang thuê,  chọn bãi xe để trả,
Mai Thị Thảo: code các module liên quan đến thông tin bãi gửi xe trong server,client, api: xem bãi gửi xe, xóa bãi gửi xe, tìm kiếm bãi gửi xe, thêm bãi gửi xe

Nguyễn Hữu Anh Việt:
	*Server: code chức năng thêm xe, xóa xe, cập nhật thông tin xe. Tạo lớp Helper để giao tiếp với (phục vụ cho) các modules khác trong server (ví dụ: thuê xe, trả xe, quản lý bãi xe)
	*Client:
		+ User: code chức năng tìm kiếm xe (xe đạp điện và xe đạp thường)
		+ Admin: code chức năng thêm, sửa, xóa, tìm kiếm xe (xe đạp điện và xe đạp thường)
	*Test: Kiểm thử hộp đen module BikeApi và hộp trắng hàm updateBike của database ở server

Nguyễn Thị Thu Thành: code các module liên quan đến quản lý thông tin thẻ tín dụng trong client,api: xem thẻ tín dụng, xóa thẻ tín dụng, tìm kiếm thẻ tín dụng, cập nhật thẻ tín dụng