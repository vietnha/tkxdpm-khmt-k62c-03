
package com.oms;

import com.oms.service.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.oms.service.bike.EBikesDataService;
import com.oms.service.bike.NormalBikesDataService;

public class OMSServer {
	public static final int PORT = 8080;
	
	public static void main(String[] args) throws Exception {
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		Server jettyServer = new Server(PORT);
		jettyServer.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);

		
		jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
				BikeBackService.class.getCanonicalName() + ", " +
				RentalService.class.getCanonicalName() + ", " +
				ParkingService.class.getCanonicalName() + ", " +
				BikeTypeService.class.getCanonicalName() + ", " +
				NormalBikeTypeService.class.getCanonicalName() + ", " +
				EBikeTypeService.class.getCanonicalName() + ", " +	
				NormalBikesDataService.class.getCanonicalName() + ", " +
				CardService.class.getCanonicalName() + ", " +
				EBikesDataService.class.getCanonicalName()
		);

		
		try {
			jettyServer.stop();
			jettyServer.start();
			jettyServer.join();
		} catch (Exception e) {
			jettyServer.stop();
			jettyServer.destroy();
		} finally {
			jettyServer.stop();
			jettyServer.destroy();
		}
	}
}