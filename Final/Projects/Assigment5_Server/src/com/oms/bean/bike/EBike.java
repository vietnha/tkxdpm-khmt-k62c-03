package com.oms.bean.bike;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class EBike extends Bike{
	private float batteryPercentage;
	private int loadCycles;
	private float estimatedUsageTimeRemaining;
	
	public EBike() {
		super();
	}
	public EBike(
		String id,
		String typeId,
		String parkingId,
		String name,
		float weight,
		String licensePlate,
		String manuafacturingDate,
		float cost,
		String producer,
		float batteryPercentage,
		int loadCycles,
		float estimatedUsageTimeRemaining
	){
		super(
			id,
			typeId,
			parkingId,
			name,
			weight,
			licensePlate,
			manuafacturingDate,
			cost,
			producer
		);
		this.batteryPercentage = batteryPercentage;
		this.loadCycles = loadCycles;
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}

	public float getBatteryPercentage() {
		return batteryPercentage;
	}
	public void setBatteryPercentage(float batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}
	public int getLoadCycles() {
		return loadCycles;
	}
	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}
	public float getEstimatedUsageTimeRemaining() {
		return estimatedUsageTimeRemaining;
	}
	public void setEstimatedUsageTimeRemaining(float estimatedUsageTimeRemaining) {
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}

	
	@Override
	public void cloneFromOtherBike(Bike bike) {
		super.cloneFromOtherBike(bike);
		if (bike instanceof EBike) {
			EBike b = (EBike) bike;
			batteryPercentage = b.batteryPercentage;
			loadCycles = b.loadCycles;
			estimatedUsageTimeRemaining = b.estimatedUsageTimeRemaining;
		}
	}
	
	@Override
	public String toString() {
		return
			super.toString() +
			"batteryPercentage: " + this.batteryPercentage +
			"loadCycles: " + this.loadCycles +
			"estimatedUsageTimeRemaining: " + this.estimatedUsageTimeRemaining;
	}
	
	@Override
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		
		
		boolean res = super.match(bike);
		if (!res) {
			return false;
		}
		
		
		if (!(bike instanceof EBike))
			return false;
		return true;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EBike) {
			return this.id.equals(((EBike) obj).id);
		}
		return false;
	}
	
	@Override
	public boolean isValidIntance() {
		boolean res = super.isValidIntance();
		if (!res) return false;
		if (this.batteryPercentage < 0 || this.batteryPercentage > 100) return false;
		if (this.loadCycles < 0) return false;
		if (this.estimatedUsageTimeRemaining < 0) return false;
		return true;
	}
}