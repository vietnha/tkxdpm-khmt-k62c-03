package com.oms.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.oms.util.log.Logger;

public class DateUtil {
	// valid if from year 1 ->
	public static boolean isValidDateYYYYMMDD(String s, boolean checkLessNowDate) { // ex "2020/02/28"
		int count = 0;
		for (int i = 0; i < s.length(); i++){
		    char c = s.charAt(i);        
		    count += (c == '/') ? 1 : 0;
		}
		if (count != 2) return false;
		String[] all = s.split("/");
		if (all.length == 3) {
			if (all[0].trim() == "" || all[0].length() > 4) return false;
			if (all[1].trim() == "" || all[1].length() > 2) return false;
			if (all[2].trim() == "" || all[2].length() > 2) return false;
			Integer y = StringUtil.getInt(all[0]);
			if (y == null || y < 1) return false;
			Integer m = StringUtil.getInt(all[1]);
			if (m == null) return false;
			Integer d = StringUtil.getInt(all[2]);
			if (d == null) return false;
			return isValidDate(y, m, d, checkLessNowDate);
		}
		return false;
	}
	public static boolean isValidDate(int y, int m, int d, boolean checkLessNowDate) {
		String strDate = Integer.toString(d) + '/' + Integer.toString(m) + '/' + Integer.toString(y);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		df.setLenient(false); 
		try {
			Date date = df.parse(strDate);
			if (date != null) {
				if (checkLessNowDate && date.after(new Date())) return false;
				return true;
			} else {
				return false;
			}
		}
		catch (Exception e) {
			return false;
		}
	}
	public static void main (String[] s) {
		System.out.print(isValidDateYYYYMMDD("2020/12/22", true));
	}
}
