package com.oms.db.bike.helper;

import java.util.ArrayList;

import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.bean.bike.NormalBike;
import com.oms.config.BikeConfig;
import com.oms.db.seed.bike.BikesData;
import com.oms.util.log.Logger;

public class JsonBikeInListHelperDatabase implements IBikeInListHelperDatabase{
	private static IBikeInListHelperDatabase singleton = new JsonBikeInListHelperDatabase();
	private ArrayList<Bike> bikes = BikesData.singleton().getAllBikes();
	
	private JsonBikeInListHelperDatabase() {
	}
	
	public static IBikeInListHelperDatabase singleton() {
		return singleton;
	}

	@Override
	public Bike getBikeById(String id) {
		for (Bike b: bikes) {
			if (b.checkBikeById(id)) {
				writeLog("getBikeById");
				return b;
			}
		}
		return null;
	}
	@Override
	public ArrayList<Bike> getBikesByParkingId(String parkingId) {
		// TODO Auto-generated method stub
		ArrayList<Bike> res = new ArrayList<Bike>();
		for (Bike b: bikes) {
			if (b.checkBikeByParkingId(parkingId)) {
				res.add(b);
			}
		}
		return res;
	}
	@Override
	public ArrayList<NormalBike> getNormalBikesByParkingId(String parkingId) {
		// TODO Auto-generated method stub
		ArrayList<NormalBike> res = new ArrayList<NormalBike>();
		for (Bike b: bikes) {
			if ((b instanceof NormalBike) && b.checkBikeByParkingId(parkingId)) {
				res.add((NormalBike)b);
			}
		}
		return res;
	}
	@Override
	public ArrayList<EBike> getEBikesByParkingId(String parkingId) {
		// TODO Auto-generated method stub
		ArrayList<EBike> res = new ArrayList<EBike>();
		for (Bike b: bikes) {
			if ((b instanceof EBike) && b.checkBikeByParkingId(parkingId)) {
				res.add((EBike)b);
			}
		}
		return res;
	}
	@Override
	public Bike updateParkingIdById(String id, String parkingId) {
		if (parkingId == null) return null;
		for (Bike b: bikes) {
			if (b.checkBikeById(id)) {
				bikes.remove(b);
				b.setParkingId(parkingId);
				bikes.add(b);
				writeLog("updatParkingIdById");
				return b;
			}
		}
		return null;
	}
	
	@Override
	public void writeLog(String description) {
		System.out.println("JsonBikeInListHelperDatabase:: SUCCESS:\t" + description);
		if (BikeConfig.enableLog) {
			Logger.printConsole(bikes);
		}
	}

	@Override
	public void writeLogToJsonFile(String nameFile) {
		// TODO Auto-generated method stub
		if (BikeConfig.enableLogJsonFileWhenCRUD) {
			Logger.printObjectToJsonFile(nameFile, bikes);
		}
	}
	
	public static void main (String[] args) {
		JsonBikeInListHelperDatabase.singleton().updateParkingIdById("ebike1", "");
		JsonBikeInListHelperDatabase.singleton().getNormalBikesByParkingId("parking1");
//		JsonBikeInListHelperDatabase.singleton().getEBikesByParkingId("parking1");
	}
}
	