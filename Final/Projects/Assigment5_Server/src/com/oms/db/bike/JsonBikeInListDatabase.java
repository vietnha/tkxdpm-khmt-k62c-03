package com.oms.db.bike;

import java.util.ArrayList;

import com.oms.bean.BikeType;
import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.bean.bike.NormalBike;
import com.oms.config.BikeConfig;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;
import com.oms.db.seed.bike.BikesData;
import com.oms.playload.bike.BikePayload;
import com.oms.playload.bike.EBikePayload;
import com.oms.playload.bike.NormalBikePayload;
import com.oms.util.log.Logger;

public class JsonBikeInListDatabase implements IBikeInListDatabase{
	private IMediaDatabase mediaDb;
	private static IBikeInListDatabase singleton = new JsonBikeInListDatabase();
	private ArrayList<Bike> bikes = BikesData.singleton().getAllBikes();
	
	private JsonBikeInListDatabase() {
		mediaDb = JsonMediaDatabase.singleton();
	}
	
	public static IBikeInListDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<? extends BikePayload> searchBike(Bike bike, BikeType bikeType) { // TODO public ArrayList<Bike> searchBike(Bike bike, BikeType bikeType) {
//		Logger.printConsole(bikeType);
		if (bike instanceof NormalBike) {
			ArrayList<NormalBikePayload> res = new ArrayList<NormalBikePayload>();
			for (Bike b: bikes) {
				bikeType.setId(b.getTypeId());
				if (b.match(bike) && mediaDb.matchBikeType(bikeType)) { // TODO if (b.match(bike) && JsonBikeTypeDatabase.singleton().match(typeId, bikeType)
//					Logger.printConsole(b);
					if (mediaDb.getInforFromID(b.getTypeId()) != null)
						res.add(new NormalBikePayload(b, mediaDb.getInforFromID(b.getTypeId())));
				}
			}
			return res;
		}
		else if (bike instanceof EBike){
			ArrayList<EBikePayload> res = new ArrayList<EBikePayload>();
			for (Bike b: bikes) {
				bikeType.setId(b.getTypeId());
				if (b.match(bike) && mediaDb.matchBikeType(bikeType)) { // TODO if (b.match(bike) && JsonBikeTypeDatabase.singleton().match(typeId, bikeType)
//					Logger.printConsole(b);
					if (mediaDb.getInforFromID(b.getTypeId()) != null)
						res.add(new EBikePayload(b, mediaDb.getInforFromID(b.getTypeId())));
				}
			}
			return res;
		}
		return null;
	}

	@Override
	public BikePayload addBike(Bike bike) {
		BikeType bikeType = mediaDb.getInforFromID(bike.getTypeId());
		if (bikeType == null || !bike.isValidIntance()) return null;
		for (Bike b: bikes) {
			if (b.equals(bike)) {
				return null;
			}
		}
		bikes.add(bike);
		writeLog("addBike");
		BikePayload res = null;
		if (bike instanceof EBike) res = new EBikePayload(bike, bikeType);
		else if (bike instanceof NormalBike) res = new NormalBikePayload(bike, bikeType);
		return res;
	}
	
	@Override
	public BikePayload removeBike(String id) {
		for (Bike b: bikes) {
			if (b.checkBikeById(id)) {
				bikes.remove(b);
				writeLog("removeBike");
				BikePayload res = null;
				if (b instanceof EBike) res = new EBikePayload(b, mediaDb.getInforFromID(b.getTypeId()));
				else if (b instanceof NormalBike) res = new NormalBikePayload(b, mediaDb.getInforFromID(b.getTypeId()));
				return res;
			}
		}
		return null;
	}
	
	@Override
	public BikePayload updateBike(Bike bike) {
		if (!bike.isValidIntance()) return null;
		BikePayload res = null;
		BikeType bikeType = mediaDb.getInforFromID(bike.getTypeId());
		if (bikeType == null) return null;
		for (Bike b: bikes) {
			if (b.equals(bike)) { // TODO if (b.equals(bike) && JsonMediaDatabase.singleton().checkExistParkingById(bike.getId()) {
				bikes.remove(b);
				b.cloneFromOtherBike(bike);
				bikes.add(b);
				if (bike instanceof EBike) res = new EBikePayload(bike, bikeType);
				else if (bike instanceof NormalBike) res = new NormalBikePayload(bike, bikeType);
				writeLog("updateBike");
				writeLogToJsonFile("bikeUpdate");
				Logger.printConsole(res);
				return res;
			}
		}
		return null;
	}
	
	@Override
	public void writeLog(String description) {
		System.out.println("JsonBikeInListDatabase:: SUCCESS:\t" + description);
		if (BikeConfig.enableLog) {
			Logger.printConsole(bikes);
		}
	}

	@Override
	public void writeLogToJsonFile(String nameFile) {
		// TODO Auto-generated method stub
		if (BikeConfig.enableLogJsonFileWhenCRUD) {
			Logger.printObjectToJsonFile(nameFile, bikes);
		}
	}
}
