package com.oms.db.bike.helper;

import com.oms.util.log.IClassLoggerable;

public interface IBikeInListHelperDatabase extends IGetNormalBikesByParkingId, IGetEBikesByParkingId, IGetBikeByIdDatabase, IGetBikesByParkingId, IUpdateParkingIdByIdDatabase, IClassLoggerable {}
