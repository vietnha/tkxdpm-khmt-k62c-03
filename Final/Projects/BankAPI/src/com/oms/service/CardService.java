package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.oms.bean.Card;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;

@Path("/cards")
public class CardService {

	private IMediaDatabase cardDatabase;

	public CardService() {
		cardDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Card> getCards(
			@QueryParam("cardID") String cardID,
			@QueryParam("name") String name,
			@QueryParam("securityCode") String securityCode,
			@QueryParam("expirationDate") String expirationDate) {
		Card card = new Card(cardID, name, securityCode,
				expirationDate);
		ArrayList<Card> res = cardDatabase.getCard(card);
		return res;
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Card> getCardsNoAgs() {
		Card card = new Card("", "", "",
				"");
		ArrayList<Card> res = cardDatabase.getCard(card);
		return res;
	}

	@GET
	@Path("/reduceMoney/{cardID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Card reduceMoney(
			@PathParam("cardID") String cardID,
			@QueryParam("money") String money) {
		System.out.println(money);
		Card card = new Card(cardID);
		Card res = cardDatabase.getCardbyId(card);
		int rsMoney = res.getMoney()
				- Integer.parseInt(money);
		if (rsMoney >= 0) {
			res.setMoney(rsMoney);
			return cardDatabase.updateCard(res);
		} else
			return null;

	}

	@POST
	@Path("/addMoney/{cardID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Card addMoney(@PathParam("cardID") String cardID,
			@QueryParam("money") String money) {
		Card card = new Card(cardID);
		Card res = cardDatabase.getCardbyId(card);
		res.setMoney(
				res.getMoney() + Integer.parseInt(money));
		return cardDatabase.updateCard(res);
	}
	
	@POST
	@Path("/match")
	@Produces(MediaType.APPLICATION_JSON)
	public Card match(Card card) {
		Card res = cardDatabase.match(card);
		return res;
	}
	
}