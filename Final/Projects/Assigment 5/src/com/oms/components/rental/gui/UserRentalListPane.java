package com.oms.components.rental.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import com.oms.bean.Card;
import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.serverapi.CardViewApi;

@SuppressWarnings("serial")
public class UserRentalListPane extends ADataListPane<RentalData> {

	public UserRentalListPane(ADataPageController<RentalData> controller) {
		this.controller = controller;
	}

	@Override
	public void decorateSinglePane(ADataSinglePane<RentalData> singlePane) {
		JButton button = new JButton("Thuê xe");
		singlePane.addDataHandlingComponent(button);

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Card> list = new CardViewApi().getAll();
				new CardListDialog(list, singlePane.getData());
			}
		});
	}
}
