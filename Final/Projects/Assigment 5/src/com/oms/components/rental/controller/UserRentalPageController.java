package com.oms.components.rental.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.rental.gui.RentalSearchPane;
import com.oms.components.rental.gui.BikeSingelPane;
import com.oms.components.rental.gui.UserRentalListPane;
import com.oms.serverapi.RentalApi;



public class UserRentalPageController extends ADataPageController<RentalData> {
	public UserRentalPageController() {
		super();
	}
	
	@Override
	public List<? extends RentalData> search(Map<String, String> searchParams) {
		return new RentalApi().getRentalData(searchParams);
	}
	
	@Override
	public BikeSingelPane createSinglePane() {
		return new BikeSingelPane();
	}
	
	@Override
	public RentalSearchPane createSearchPane() {
		return new RentalSearchPane();
	}

	@Override
	public ADataListPane<RentalData> createListPane() {
		return new UserRentalListPane(this);
	}
	
}
