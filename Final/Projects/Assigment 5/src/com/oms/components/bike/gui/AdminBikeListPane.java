package com.oms.components.bike.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.bean.bike.NormalBike;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.bike.BikeState;
import com.oms.components.bike.controller.AdminBikePageController;
import com.oms.components.bike.ebike.EBikeCreateDialog;
import com.oms.components.bike.ebike.EBikeEditDialog;


@SuppressWarnings("serial")

public class AdminBikeListPane extends ADataListPane<Bike>{
	private JButton btnAdd;
	
	public AdminBikeListPane(ADataPageController<Bike> controller) {
		this.controller = controller;
		AdminBikeListPane self = this;
		IDataManageController<Bike> manageController = new IDataManageController<Bike>() {
			@Override
			public void update(Bike t) {
			}

			@SuppressWarnings("unchecked")
			@Override
			public void create(Bike t) {
				if (controller instanceof AdminBikePageController) {
					Bike bike = ((AdminBikePageController) controller).create(t);
					if (bike != null) {
						if (bike instanceof NormalBike) {
							bike = (NormalBike) bike;
							((List<NormalBike>)list).add((NormalBike) bike);
						}
						else if (bike instanceof EBike){
							bike = (EBike) bike;
							((List<EBike>)list).add((EBike) bike);
						}
						self.updateData(self.list);
					}
				}
			}

			@Override
			public void read(Bike t) {
			}

			@Override
			public void delete(Bike t) {
			}
		};
		
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (BikeState.getInstance().getStateTab()) {
					case BikeState.NORMALBIKE:
						new BikeCreateDialog(new NormalBike(), manageController);
						break;
					case BikeState.EBIKE:
						new EBikeCreateDialog(new EBike(), manageController);
						break;
				}
			}
		});
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Bike> singlePane) {
		JButton button = new JButton("Edit");
		singlePane.addDataHandlingComponent(button);
		
		JButton button1 = new JButton("Remove");
		singlePane.addDataHandlingComponent(button1);
		
		JButton button2 = new JButton("Add");
		singlePane.addDataHandlingComponent(button2);
		
		AdminBikeListPane self = this;
		IDataManageController<Bike> manageController = new IDataManageController<Bike>() {
			@Override
			public void update(Bike t) {
				if (controller instanceof AdminBikePageController) {
					Bike newBike = ((AdminBikePageController) controller).updateBike(t);
					if (newBike != null) singlePane.updateData(newBike);
				}
			}

			@SuppressWarnings("unchecked")
			@Override
			public void create(Bike t) {
				if (controller instanceof AdminBikePageController) {
					((AdminBikePageController) controller).create(t);
					if (t != null) {
						if (t instanceof NormalBike) {
							t = (NormalBike) t;
							((List<NormalBike>)list).add((NormalBike) t);
						}
						else if (t instanceof EBike) {
							t = (EBike) t;
							((List<EBike>)list).add((EBike) t);
						}
						self.updateData(self.list);
					}
				}
			}

			@Override
			public void read(Bike t) {
			}

			@Override
			public void delete(Bike t) {
				if (controller instanceof AdminBikePageController) {
					Bike bike = ((AdminBikePageController) controller).removeBike(t);
					if (bike != null) {
						self.list.remove(bike);
						self.updateData(self.list);
					}
				}
			}
		};
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (BikeState.getInstance().getStateTab()) {
					case BikeState.NORMALBIKE:
						new BikeEditDialog(singlePane.getData(), manageController);
						break;
					case BikeState.EBIKE:
						new EBikeEditDialog(singlePane.getData(), manageController);
						break;
				}
			}
		});
		
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manageController.delete(singlePane.getData());
//				self.handleDeleteSinglePane(singlePane.getData());
			}
		});
		
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (BikeState.getInstance().getStateTab()) {
					case BikeState.NORMALBIKE:
						NormalBike normalBike = new NormalBike();
						normalBike.cloneFromOtherBike(singlePane.getData());
						new BikeCreateDialog(normalBike, manageController);
						break;
					case BikeState.EBIKE:
						EBike ebike = new EBike();
						ebike.cloneFromOtherBike(singlePane.getData());
						new EBikeCreateDialog(ebike, manageController);
						break;
				}
			}
		});	
	}
	
	public void handleDeleteSinglePane(Bike bike) {
		list.remove(bike);
		this.updateData(this.list);
	}
	
	@Override
	public void updateData(List<? extends Bike> list) {
		super.updateData(list);
		if (list.size() <= 0) pane.add(btnAdd);
	}
}

