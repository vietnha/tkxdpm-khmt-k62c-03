
package com.oms.components.parking.gui;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.gui.ADataSearchPane;



@SuppressWarnings("serial")
public class ParkingSearchPane extends ADataSearchPane {	
	private JTextField nameField;
	private JTextField addressField;
	private JTextField distanceField;
	private JTextField timeField;

	public ParkingSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Name");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		JLabel addressLabel = new JLabel("Address");
		addressField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(addressLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(addressField, c);
		
		JLabel distanceLabel = new JLabel("Distance");
		distanceField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(distanceLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(distanceField, c);
		
		JLabel timeLabel = new JLabel("Time");
		timeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(timeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(timeField, c);

	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!nameField.getText().trim().equals("")) {
		res.put("name", nameField.getText().trim());
	    }
		if (!addressField.getText().trim().equals("")) {
			res.put("address", addressField.getText().trim());
		}
		if (!distanceField.getText().trim().equals("")) {
			res.put("distance", distanceField.getText().trim());
		}
		if (!timeField.getText().trim().equals("")) {
			res.put("time", timeField.getText().trim());
		}		
		return res;
	}
}
