
package com.oms.components.parking.gui;
import javax.swing.JLabel;

import com.oms.bean.Parking;
import com.oms.components.abstractdata.gui.ADataSinglePane;


@SuppressWarnings("serial")
public class ParkingSingelPane extends ADataSinglePane<Parking>{
	private JLabel labelName;
	private JLabel labelAddress;
	private JLabel labelDistance;
	private JLabel labelTime;
	private JLabel labelNumberOfBikes;
	private JLabel labelNumberOfeBikes;
	private JLabel labelNumberOfTwinBikes;
	private JLabel labelNumberOfEmtyDocks;
	
	public ParkingSingelPane() {
		super();
	}
		
	
	public ParkingSingelPane(Parking parking) {
		this();
		this.t = parking;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
	int row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelName = new JLabel();
	add(labelName, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelAddress = new JLabel();
	add(labelAddress, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelDistance = new JLabel();
	add(labelDistance, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelTime = new JLabel();
	add(labelTime, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfBikes = new JLabel();
	add(labelNumberOfBikes, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfeBikes = new JLabel();
	add(labelNumberOfeBikes, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfTwinBikes = new JLabel();
	add(labelNumberOfTwinBikes, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfEmtyDocks = new JLabel();
	add(labelNumberOfEmtyDocks, c);

	}
	
	
	@Override
	public void displayData() {
		labelName.setText("Name of parking: " + t.getName());
		labelAddress.setText("Address of parking: " + t.getAddress());
		labelDistance.setText("Distance to parking: " + t.getDistance()+" km");
		labelNumberOfBikes.setText("Number Of Bikes: " + t.getNumberOfBikes());
		labelNumberOfeBikes.setText("Number Of eBikes : " + t.getNumberOfeBikes());
		labelNumberOfTwinBikes.setText("Number Of Twi Bikes : " + t.getNumberOfTwinBikes());
		labelNumberOfEmtyDocks.setText("NumberOfEmtyDock: " + t.getNumberOfEmtyDocks());
		labelTime.setText("Time to parking: " + t.getTime()+" phút");

	}
}
