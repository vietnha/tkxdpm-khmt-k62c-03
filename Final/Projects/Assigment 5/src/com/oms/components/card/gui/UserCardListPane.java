package com.oms.components.card.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;

import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.card.controller.UserCardPageController;
import com.oms.serverapi.CardApi;



@SuppressWarnings("serial")

public class UserCardListPane extends ADataListPane<Card>{
	
	public UserCardListPane(ADataPageController<Card> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Card> singlePane) {
		JButton editButton = new JButton("Edit");
		singlePane.addDataHandlingComponent(editButton);
		JButton deleteButton = new JButton("delete");
		singlePane.addDataHandlingComponent(deleteButton);
		
		IDataManageController<Card> manageController = new IDataManageController<Card>() {
			@Override
			public void update(Card t) {
				if (controller instanceof UserCardPageController) {
					Card newCard = ((UserCardPageController) controller).updateCard(t);
					singlePane.updateData(newCard);
				}
			}

			@Override
			public void create(Card t) {
			}

			@Override
			public void read(Card t) {
			}

			@Override
			public void delete(Card t) {
				if (controller instanceof UserCardPageController) {
					((UserCardPageController) controller).deleteCard(t);
					singlePane.setVisible(false);
				}
			}
		};
		
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new CardEditDialog(singlePane.getData(), manageController);
			}
		});	
		
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manageController.delete(singlePane.getData());
			}
		});	
	}
}

