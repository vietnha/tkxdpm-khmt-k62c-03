
package com.oms.components.card.gui;
import javax.swing.JLabel;

import com.oms.bean.Card;
import com.oms.components.abstractdata.gui.ADataSinglePane;


@SuppressWarnings("serial")
public class CardSingelPane extends ADataSinglePane<Card>{
	private JLabel labelName;
	private JLabel labelSecurityCode;
	private JLabel labelExprirationDate;
		
	public CardSingelPane() {
		super();
	}
		
	
	public CardSingelPane(Card card) {
		this();
		this.t = card;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
	int row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelName = new JLabel();
	add(labelName, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelSecurityCode = new JLabel();
	add(labelSecurityCode, c);

	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelExprirationDate = new JLabel();
	add(labelExprirationDate, c);
	

	}
	
	
	@Override
	public void displayData() {
		labelName.setText("Tên chủ thẻ: " + t.getName());
		labelSecurityCode.setText("Mã bảo mật: " + t.getSecurityCode());
		labelExprirationDate.setText("Ngày hết hạn: " + t.getExprirationDate());
	}
}
