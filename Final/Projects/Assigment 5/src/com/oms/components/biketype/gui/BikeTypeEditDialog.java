package com.oms.components.biketype.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.BikeType;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataEditDialog;

@SuppressWarnings("serial")
public class BikeTypeEditDialog extends ADataEditDialog<BikeType>{
	
	private JTextField nameField;
	private JTextField soYenField;
	private JTextField tienCocField;
	private JTextField soGheSauField;
	private JTextField mucGiaField;
	
	public BikeTypeEditDialog(BikeType biketype, IDataManageController<BikeType> controller) {
		super(biketype, controller);
	}

	@Override
	public void buildControls() {
		nameField = addSlot("Name", t.getName());
		soYenField = addSlot("So Yen", Integer.toString(t.getSo_yen()));
		tienCocField = addSlot("Tien Coc", Integer.toString(t.getTien_coc()));
		soGheSauField= addSlot("So Ghe Sau", Integer.toString(t.getSo_ghe_sau()));
		mucGiaField= addSlot("Muc gia", Float.toString(t.getMuc_gia()));
	}
	
	public JTextField addSlot(String labelTitle, String text) {
		int row = getLastRowIndex();
		JLabel label = new JLabel(labelTitle);
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(label, c);
		JTextField field = new JTextField(15);
		field.setText(text);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(field, c);
		return field;
	}

	@Override
	public BikeType getNewData() {
		t.setName(nameField.getText());
		t.setSo_yen(Integer.parseInt(soYenField.getText()));
		t.setTien_coc(Integer.parseInt(tienCocField.getText()));
		t.setSo_ghe_sau(Integer.parseInt(soGheSauField.getText()));
		t.setMuc_gia(Float.parseFloat(mucGiaField.getText()));
		
		return t;
	}
}
