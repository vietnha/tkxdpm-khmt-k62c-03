package com.oms.components.biketype.normalbiketype.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.biketype.gui.BikeTypeSearchPane;

@SuppressWarnings("serial")
public class NormalBikeTypeSearchPane extends BikeTypeSearchPane {

	public NormalBikeTypeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
	}
	
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		return res;
	}
}
