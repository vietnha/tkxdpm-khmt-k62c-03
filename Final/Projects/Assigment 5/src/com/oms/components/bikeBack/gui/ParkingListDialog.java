package com.oms.components.bikeBack.gui;

import com.oms.bean.BikeBack;
import com.oms.bean.Card;
import com.oms.bean.Parking;
import com.oms.bean.RentalData;

import javax.swing.*;
import java.util.List;

@SuppressWarnings("serial")
public class ParkingListDialog extends ABikeBackDataListDialog<Parking> {

	public ParkingListDialog(List<Parking> parkings, BikeBack bikeBack) {
		super(parkings, bikeBack);
	}

	@Override
	public void buildControls(int i) {
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Id of Parking: " + t.get(i).getId());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);

		row = getLastRowIndex();
		JLabel labelName = new JLabel("Name of parking: " + t.get(i).getName());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(labelName, c);
		
		row = getLastRowIndex();
		JLabel labelAddress = new JLabel("Address of parking: " + t.get(i).getAddress());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(labelAddress, c);
		
		row = getLastRowIndex();
		JLabel labelDistance = new JLabel("Distance to parking: " + t.get(i).getDistance());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(labelDistance, c);
		
		row = getLastRowIndex();
		JLabel labelTime = new JLabel("Time to parking: " + t.get(i).getTime());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(labelTime, c);
	}

	@Override
	public Parking getNewData(int i) {
		return t.get(i);
	}

	@Override
	public String getParkingId(int i) {
		return t.get(i).getId();
	}

}

