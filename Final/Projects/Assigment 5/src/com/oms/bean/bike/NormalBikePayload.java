package com.oms.bean.bike;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonDeserialize
@JsonSerialize
public class NormalBikePayload extends BikePayload {
	public NormalBikePayload() {
		super();
	}

	public NormalBikePayload(
			String id,
			String typeId,
			String parkingId,
			String name,
			float weight,
			String licensePlate,
			String manuafacturingDate,
			float cost,
			String producer
	){
		super(
			id,
			typeId,
			parkingId,
			name,
			weight,
			licensePlate,
			manuafacturingDate,
			cost,
			producer
		);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NormalBikePayload) {
			return this.id.equals(((NormalBikePayload) obj).id);
		}
		return false;
	}
}
