package com.oms.bean.bike;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("bike")
@JsonSubTypes({ @Type(value = NormalBikePayload.class, name = "normalbike"), @Type(value = EBikePayload.class, name = "ebike") })
public abstract class BikePayload {
	protected String id;
	protected String typeId;
	protected String parkingId;
	protected String name;
	protected float weight;
	protected String licensePlate;
	protected String manuafacturingDate;
	protected float cost;
	protected String producer;
	
	public BikePayload() {
		super();
	}
	public BikePayload(
		String id,
		String typeId,
		String parkingId,
		String name,
		float weight,
		String licensePlate,
		String manuafacturingDate,
		float cost,
		String producer
	){
		this.id = id;
		this.typeId = typeId;
		this.parkingId = parkingId;
		this.name = name;
		this.weight = weight;
		this.cost = cost;
		this.producer = producer;
		this.manuafacturingDate = manuafacturingDate;
		this.licensePlate = licensePlate;
	}

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getParkingId() {
		return parkingId;
	}
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getManuafacturingDate() {
		return manuafacturingDate;
	}
	public void setManuafacturingDate(String manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	
	public BikePayload cloneFromBike(Bike bike) {
		id = bike.getId();
		typeId = bike.getTypeId();
		parkingId = bike.getParkingId();
		name = bike.getName();
		weight = bike.getWeight();
		licensePlate = bike.getLicensePlate();
		manuafacturingDate = bike.getManuafacturingDate();
		cost = bike.getCost();
		producer = bike.getProducer();
		return this;
	}
	
	@Override
	public String toString() {
		return
			"id: " + this.id +
			"typeId: " + this.typeId +
			"parkingId: " + this.parkingId +
			"name: " + this.name +
			"weight: " + this.weight +
			"licensePlate: " + this.licensePlate +
			"manuafacturingDate: " + this.manuafacturingDate +
			"cost: " + this.cost +
			"producer: " + this.producer;
	}	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BikePayload) {
			return this.id.equals(((BikePayload) obj).id);
		}
		return false;
	}
}