package com.oms.bean.bike;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonDeserialize
@JsonSerialize
public class EBikePayload extends BikePayload{
	private float batteryPercentage;
	private int loadCycles;
	private float estimatedUsageTimeRemaining;
	
	public EBikePayload() {
		super();
	}
	public EBikePayload(
		String id,
		String typeId,
		String parkingId,
		String name,
		float weight,
		String licensePlate,
		String manuafacturingDate,
		float cost,
		String producer,
		float batteryPercentage,
		int loadCycles,
		float estimatedUsageTimeRemaining
	){
		super(
			id,
			typeId,
			parkingId,
			name,
			weight,
			licensePlate,
			manuafacturingDate,
			cost,
			producer
		);
		this.batteryPercentage = batteryPercentage;
		this.loadCycles = loadCycles;
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}

	public float getBatteryPercentage() {
		return batteryPercentage;
	}
	public void setBatteryPercentage(float batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}
	public int getLoadCycles() {
		return loadCycles;
	}
	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}
	public float getEstimatedUsageTimeRemaining() {
		return estimatedUsageTimeRemaining;
	}
	public void setEstimatedUsageTimeRemaining(float estimatedUsageTimeRemaining) {
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}
	
	@Override
	public BikePayload cloneFromBike(Bike bike) {
		super.cloneFromBike(bike);
		this.batteryPercentage = ((EBike) bike).getBatteryPercentage();
		this.loadCycles = ((EBike) bike).getLoadCycles();
		this.estimatedUsageTimeRemaining = ((EBike) bike).getEstimatedUsageTimeRemaining();

		return this;
	}
	
	@Override
	public String toString() {
		return
			super.toString() +
			"batteryPercentage: " + this.batteryPercentage +
			"loadCycles: " + this.loadCycles +
			"estimatedUsageTimeRemaining: " + this.estimatedUsageTimeRemaining;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EBike) {
			return this.id.equals(((EBike) obj).id);
		}
		return false;
	}
}