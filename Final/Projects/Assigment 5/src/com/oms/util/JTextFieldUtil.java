package com.oms.util;

import javax.swing.JTextField;

public class JTextFieldUtil {
	public static float getFloatIfSuccess(JTextField j, float f) {
		Float temp = StringUtil.getFloat(j.getText().trim());
		return temp == null ? f : temp;
	}
	public static int getIntegerIfSuccess(JTextField j, int i) {
		Integer temp = StringUtil.getInt(j.getText().trim());
		return temp == null ? i : temp;
	}
}