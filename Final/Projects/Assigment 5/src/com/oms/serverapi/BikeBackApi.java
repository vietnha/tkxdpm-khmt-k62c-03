package com.oms.serverapi;

import com.oms.bean.BikeBack;
import com.oms.bean.RentalData;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Map;


public class BikeBackApi {
	public static final String PATH = "http://localhost:8080/";

	private Client client;

	public BikeBackApi() {
		client = ClientBuilder.newClient();
	}

	public ArrayList<BikeBack> getRentalDatas(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("bikeBack").path("rentalDatas");

		if (queryParams != null) {
			if (!queryParams.containsKey("id"))
				return new ArrayList<BikeBack>();
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<BikeBack> res = response.readEntity(new GenericType<ArrayList<BikeBack>>() {});
		return res;
	}

	public RentalData bikeBackApi(BikeBack bikeBack, String parkingId) {
		System.out.println(bikeBack.getId());
		System.out.println(bikeBack.getCardId());
		System.out.println(parkingId);
		WebTarget webTarget = client.target(PATH).path("bikeBack").path(bikeBack.getId()).path(bikeBack.getCardId()).path(parkingId);

		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(bikeBack, MediaType.APPLICATION_JSON));
		return null;
	}
}
