package com.oms.serverapi.test.biketype;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ BlackBoxUpdateEBikeTypeApiTest.class, WhiteBoxUpdateEBikeTypeApiTest.class })
public class BikeTypeApiTest {

}