package com.oms.serverapi.test.bike;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.serverapi.BikeApi;
import com.oms.util.log.Logger;

@RunWith(Parameterized.class)
public class ParameterizedEBikeApiTest {
	private final static EBike VALID = new EBike("ebike1", "ebike1", "parking2", "Asama 2022", 1.0f, "2010574", "2020/11/13", 250000, "eVINX", 90, 2, 3, "", 0, 0, 0, 0);
	
	private EBike input;
	private EBike expected;
	
	public ParameterizedEBikeApiTest(
		EBike input,
		EBike expected
	) {
		this.input = input;
		this.expected = expected;
	}
	
	// All Test Cases
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{ // TC1: TypeId hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetTypeId("ebike1"),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetTypeId("ebike1"),
			},
			{ // TC2: TypeId khong co trong danh sach
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetTypeId("ebik"),
				null
			},
			{ // TC3: ParkingId hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetParkingId("parking3"),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetParkingId("parking3"),
			},
			{ // TC4: ParkingId null
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetParkingId(null),
				null
			},
//			{ // TC5: Name hop le
//				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetName("SH 2020"),
//				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetName("SH 2020"),
//			},
//			{ // TC6: Name rong
//				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetName(" "),
//				null
//			},
			{ // TC7: Name null
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetName(null),
				null
			},
			{ // TC8: Weight hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetWeight(2.8f),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetWeight(2.8f),
			},
			{ // TC9: Weight = 0 hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetWeight(0),
				null
			},
			{ // TC9: Weight < 0 invalid
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetWeight(-2.8f),
				null
			},
			{ // TC10: LicensePlate hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLicensePlate("ss"),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLicensePlate("ss"),
			},
			{ // TC11: LicensePlate rong
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLicensePlate(""),
				null
			},
			{ // TC12: LicensePlate null
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLicensePlate(null),
				null
			},
			{ // TC13: Cost hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetCost(2.8f),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetCost(2.8f),
			},
			{ // TC14: Cost = 0 valid
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetCost(0),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetCost(0),
			},
			{ // TC15: Cost < 0 invalid
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetCost(-2.8f),
				null
			},
			{ // TC16: Producer hop le
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetProducer("ss"),
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetProducer("ss"),
			},
			{ // TC17: Producer rong
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetProducer(""),
				null
			},
			{ // TC18: Producer null
				(new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetProducer(null),
				null
			},
			{ // TC19: BatteryPercentage hop le
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(2.8f),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(2.8f),
			},
			{ // TC20: BatteryPercentage = 0 valid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(0),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(0)
			},
			{ // TC21: BatteryPercentage < 0 invalid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(-2.8f),
				null
			},
			{ // TC22: BatteryPercentage = 100 valid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(100),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(100)
			},
			{ // TC23: BatteryPercentage > 100 invalid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetBatteryPercentage(101),
				null
			},
			{ // TC24: LoadCycles hop le
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLoadCycles(2),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLoadCycles(2),
			},
			{ // TC25: LoadCycles = 0 valid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLoadCycles(0),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLoadCycles(0)
			},
			{ // TC26: LoadCycles < 0 invalid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetLoadCycles(-2),
				null
			},
			{ // TC27: EstimatedUsageTimeRemaining hop le
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetEstimatedUsageTimeRemaining(2),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetEstimatedUsageTimeRemaining(2),
			},
			{ // TC28: EstimatedUsageTimeRemaining = 0 valid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetEstimatedUsageTimeRemaining(0),
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetEstimatedUsageTimeRemaining(0)
			},
			{ // TC29: EstimatedUsageTimeRemaining < 0 invalid
				((EBike) new EBike().cloneFromOtherBikeAndReturn(VALID)).setGetEstimatedUsageTimeRemaining(-2),
				null
			},
			{ // TC30: ManuafacturingDate hop le ( < today date )
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/11/3"),
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/11/3"),
			},
			{ // TC31: ManuafacturingDate invalid nam khong nhuan
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2019/02/29"),
				null
			},
			{ // TC31: ManuafacturingDate valid nam nhuan
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/02/29"),
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/02/29")
			},
			{ // TC32: ManuafacturingDate date > today date valid
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/12/23"),
				null
			},
			{ // TC33: ManuafacturingDate date = today date valid
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/12/22"),
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/12/22")
			},
			{ // TC34: ManuafacturingDate sai so thang
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2019/13/12"),
				null
			},
			{ // TC35: ManuafacturingDate sai nam
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("0/1/12"),
				null
			},
			{ // TC36: ManuafacturingDate sai ngay
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/4/31"),
				null
			},
			{ // TC37: ManuafacturingDate sai format
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/4/"),
				null
			},
			{ // TC38: ManuafacturingDate sai format 2
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/4/ 29"),
				null
			},
			{ // TC39: ManuafacturingDate sai format 3
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("2020/4/-4"),
				null
			},
			{ // TC40: ManuafacturingDate sai format 4
				new EBike().cloneFromOtherBikeAndReturn(VALID).setGetManuafacturingDate("20204"),
				null
			},
		});
	}
	
	@Test
	public void testUpdateApi() {		
		EBike result = (EBike) BikeApi.singleton().updateBike(input);
		EBike b1 = result;
		EBike b2 = expected;
		String err = "Error BikeApi::updateBike(EBike) !!!";
		if (b2 == null) assertEquals(err, b1, b2);
		else {
			assertEquals(err, b2.getId() , b1.getId());
			assertEquals(err, b2.getTypeId() , b1.getTypeId());
			assertEquals(err, b2.getParkingId() , b1.getParkingId());
			assertEquals(err, b2.getName() , b1.getName());
			assertEquals(err, b2.getWeight() , b1.getWeight(), 0.01f);
			assertEquals(err, b2.getLicensePlate() , b1.getLicensePlate());
			assertEquals(err, b2.getManuafacturingDate() , b1.getManuafacturingDate());
			assertEquals(err, b2.getCost() , b1.getCost(), 0.01f);
			assertEquals(err, b2.getProducer() , b1.getProducer());
			assertEquals(err, b2.getBatteryPercentage() , b1.getBatteryPercentage(), 0.01f);
			assertEquals(err, b2.getLoadCycles() , b1.getLoadCycles());
			assertEquals(err, b2.getEstimatedUsageTimeRemaining() , b1.getEstimatedUsageTimeRemaining(), 0.01f);
		}
	}
}