package com.oms.serverapi.test.parking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.oms.bean.Parking;
import com.oms.serverapi.ParkingApi;

@RunWith(Parameterized.class)

public class ParameterizedParkingApiTest {
	private String parkingName;
	private String expectedResult;
	
	private ParkingApi api = new ParkingApi();
	
	
	public ParameterizedParkingApiTest(String parkingName, String expectedResult) {
		super();
		this.parkingName = parkingName;
		this.expectedResult = expectedResult;
	}
	

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{ "A", "A bai do xe A" },
			{ "B", "B bai do xe B" }, 
			
			{ "C", "bai do xe C" }
			
		});
	}
	
	@Test
	public void testGetParking() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", parkingName);
		
		ArrayList<Parking> list= api.getParking(params);
		assertTrue("No data", list.size() > 0);
		
		
		Parking parking = list.get(0);
		assertEquals("Eror in getBooks API!", parking.getName(), expectedResult);
	}

}
