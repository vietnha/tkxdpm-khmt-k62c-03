package com.oms.serverapi.test.bike;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ EBikeApiTest.class, ParameterizedEBikeApiTest.class })
public class BikeApiTests {

}