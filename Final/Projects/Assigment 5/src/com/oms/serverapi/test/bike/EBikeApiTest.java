package com.oms.serverapi.test.bike;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.oms.bean.bike.EBike;
import com.oms.serverapi.BikeApi;
import com.oms.util.log.Logger;

public class EBikeApiTest {
	private BikeApi api = BikeApi.singleton();
	
	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateTypeIdEquivalent() {
		ArrayList<EBike> list = (ArrayList<EBike>) api.getBikes(null, BikeApi.PATH_EBIKE);
		assertTrue("No data", list.size() > 0);
		
		EBike bike = list.get(0);
//		Logger.printConsole(bike);
		String newTypeId = "normalbike2";
		bike.setTypeId(newTypeId);
		bike = (EBike) api.updateBike(bike);
		assertTrue("Error in updateBike API!", bike != null);
//		Logger.printConsole(bike);
//		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("typeId", newTypeId);
		list = (ArrayList<EBike>) api.getBikes(params, BikeApi.PATH_EBIKE);
		assertTrue("Error in updateBike API!", list.size() > 0);
		
		EBike newEBike = (EBike) (api.getBikes(params, BikeApi.PATH_EBIKE).get(0));
		assertEquals("Error in updateBike API!", newEBike.getTypeId(), newTypeId);
	}
}
