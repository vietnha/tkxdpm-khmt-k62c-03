package com.oms.serverapi.test.biketype;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.oms.bean.EBikeType;
import com.oms.bean.BikeType;
import com.oms.serverapi.BikeTypeApi;


public class BlackBoxUpdateEBikeTypeApiTest {
	private BikeTypeApi api = new BikeTypeApi();
	
//	Test update EbikeType null
	@Test
	public void testUpdateEBikeType1() {
		
		EBikeType res = api.updateEBikeType(null);
		
		
		assertTrue("Eror in updateEBikeType API!", res==null);
		
	}
	
	
//	Test update EbikeType chỉ có trường id:
	@Test
	public void testUpdateEBikeType2() {
		ArrayList<EBikeType> list= api.getEBikeTypes(null);
		assertTrue("No data", list.size() > 0);
		
		
		EBikeType ebiketype= list.get(0);
		String newName= "xe dap don dien 2";
		ebiketype.setName(newName);
		api.updateEBikeType(ebiketype);
		String check = ebiketype.getId();
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", newName);
		list = api.getEBikeTypes(params);
		assertTrue("Eror in updateEBikeType API!", list.size() > 0);
		
	
		EBikeType newEBikeType = api.getEBikeTypes(params).get(0);
		assertEquals("Eror in updateEBikeType API!", newEBikeType.getId(), ebiketype.getId());
	}
	
	
	// Test update EbikeType không có trường “id”:
	@Test
	public void testUpdateEBikeType3() {
		
		EBikeType ebiketype= new EBikeType();
		String newName= "xe dap don dien 2";
		int so_yen = 3;
		ebiketype.setName(newName);
		ebiketype.setSo_yen(so_yen);
		EBikeType res = api.updateEBikeType(ebiketype);
		
		assertTrue("Eror in updateEBikeType API!",res == null);
		
	}
	
	//Test update EbikeType có trường “id” không nằm trong database:
	@Test
	public void testUpdateEBikeType4() {
		
		EBikeType ebiketype= new EBikeType();
		String newName= "xe dap don dien 2";
		String id = "bike12";
		ebiketype.setName(newName);
		ebiketype.setId(id);
		EBikeType res = api.updateEBikeType(ebiketype);
		
		assertTrue("Eror in updateEBikeType API!",res == null);
		
	}
	
	//Test update EbikeType có trường “id” nằm trong database và trường “name”, trường “so_gio_sac”:
	@Test
	public void testUpdateEBikeType5() {
		
		EBikeType ebiketype= new EBikeType();
		String newName= "xe dap don dien 2";
		String id = "ebike1";
		float so_gio_sac = 1.5f;
		ebiketype.setName(newName);
		ebiketype.setSo_gio_sac(so_gio_sac);
		ebiketype.setId(id);
		EBikeType res = api.updateEBikeType(ebiketype);
		
		assertTrue("Eror in updateEBikeType API!",res != null);
		assertEquals("Eror in updateEBikeType API!", res.getName(), newName);
		assertTrue("Eror in updateEBikeType API!", (res.getSo_gio_sac() - so_gio_sac)==0);
		
	}
	
	//Test update EbikeType có trường id = “”:
	@Test
	public void testUpdateEBikeType6() {
		
		EBikeType ebiketype= new EBikeType();
		
		String id = "";
		ebiketype.setId(id);
		EBikeType res = api.updateEBikeType(ebiketype);
		
		assertTrue("Eror in updateEBikeType API!",res == null);
		
	}
	
	//Test update EbikeType chỉ có các trường khác id:
	@Test
	public void testUpdateEBikeType7() {
		
		EBikeType ebiketype= new EBikeType();
		String newName= "xe dap don";
		int so_yen = 13;
		ebiketype.setName(newName);
		ebiketype.setSo_yen(so_yen);
		EBikeType res = api.updateEBikeType(ebiketype);
		
		assertTrue("Eror in updateEBikeType API!",res == null);
		
	}
	
//	@Test
//	public void testUpdateEBikeType8() {
//		ArrayList<EBikeType> list= api.getEBikeTypes(null);
//		assertTrue("No data", list.size() > 0);
//		
//		
//		EBikeType ebiketype= list.get(0);
//		String newName= "xe dap don dien 2";
//		ebiketype.setName(newName);
//		api.updateEBikeType(ebiketype);
//		
//		
//		HashMap<String, String> params = new HashMap<String, String>();
//		params.put("name", newName);
//		
//		list = api.getEBikeTypes(params);
//		assertTrue("Eror in updateEBikeType API!", list.size() > 0);
//		
//	
//		EBikeType newEBikeType = api.getEBikeTypes(params).get(0);
//		assertEquals("Eror in updateEBikeType API!", newEBikeType.getName(), newName);
//	}

}
