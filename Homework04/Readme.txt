Cả nhóm họp lần 4 qua MS Team ngày 3/11/2020 để thảo luận về bài tập 4. 
Nội dung họp : gộp lại các biểu đồ lớp thiết kế của mỗi thành viên, tổ 
chức thành các package cho hợp lý, thống nhất cách thức đặt tên. Sau 
đó, các thành viên tiến hành vẽ biểu đồ package cho từng cá nhân. Một 
bạn vẽ biểu đồ package cho toàn nhóm, các bạn còn lại góp ý sửa đổi bổ 
sung cho hợp lý.