package com.oms.db;

import java.util.ArrayList;
import com.oms.bean.Card;
import com.oms.db.seed.Seed;

public class JsonMediaDatabase implements IMediaDatabase{
	private static IMediaDatabase singleton = new JsonMediaDatabase();
	private ArrayList<Card> cards = Seed.singleton().getCard();
	
	private JsonMediaDatabase() {
	}
	
	public static IMediaDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Card> searchCard(Card card) {
		ArrayList<Card> res = new ArrayList<Card>();
		for (Card b: cards) {
			if (b.match(card)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public Card addCard(Card card) {
		for (Card m: cards) {
			if (m.equals(card)) {
				return null;
			}
		}
		
		cards.add(card);
		return card;
	}
	
	@Override
	public Card updateCard(Card card) {
		for (Card m: cards) {
			if (m.equals(card)) {
				cards.remove(m);
				cards.add(card);
				return card;
			}
		}
		return null;
	}

	@Override
	public Card deleteCard(Card card) {
		for (Card m: cards) {
			if (m.equals(card)) {
				cards.remove(m);
				return card;
			}
		}
		return null;
	}
}
