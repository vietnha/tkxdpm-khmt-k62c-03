package com.oms.serverapi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.oms.bean.Card;
import com.oms.serverapi.CardApi;


public class CardApiTest {
	private CardApi api = new CardApi();
	@Test
	public void testGetAllCard() {
		ArrayList<Card> list= api.getCard(null);
		assertEquals("Error in getCard API!", list.size(), 3);
	}
	
	@Test(timeout = 1000)
	public void testResponse() {
		api.getCard(null);
	}
	
	@Test
	public void testUpdateCard() {
		ArrayList<Card> list= api.getCard(null);
		assertTrue("No data", list.size() > 0);
		
		
		Card card = list.get(0);
		String newName= "O'Reilly";
		card.setName(newName);
		api.updateCard(card);
		
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("Name", newName);
		list = api.getCard(params);
		assertTrue("Eror in updateBook API!", list.size() > 0);
		
	
		Card newCard = api.getCard(params).get(0);
		assertEquals("Eror in updateCard API!", newCard.getName(), newName);
	}

}
