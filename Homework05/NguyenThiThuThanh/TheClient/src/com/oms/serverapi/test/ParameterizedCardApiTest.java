package com.oms.serverapi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.oms.bean.Card;
import com.oms.serverapi.CardApi;

@RunWith(Parameterized.class)

public class ParameterizedCardApiTest {
	private String cardName;
	private String expectedResult;
	
	private CardApi api = new CardApi();
	
	
	public ParameterizedCardApiTest(String cardName, String expectedResult) {
		super();
		this.cardName = cardName;
		this.expectedResult = expectedResult;
	}
	

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{ "A", "A bãi đỗ xe tạ quang bửu" },
			{ "B", "B bãi đỗ xe bách khoa" }, 
			
			{ "C", "C bãi đỗ xe hoàng hà" },
			{ "D", "D bãi đỗ xe xuân hòa" }
		});
	}
	
	@Test
	public void testGetCard() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", cardName);
		
		ArrayList<Card> list= api.getCard(params);
		assertTrue("No data", list.size() > 0);
		
		
		Card card = list.get(0);
		assertEquals("Eror in getBooks API!", card.getName(), expectedResult);
	}

}
