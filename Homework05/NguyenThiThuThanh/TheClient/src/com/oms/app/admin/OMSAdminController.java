package com.oms.app.admin;

import javax.swing.JPanel;
import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.card.controller.AdminCardPageController;



public class OMSAdminController {
	public OMSAdminController() {
	}
	
	public JPanel getCardPage() {
		ADataPageController<Card> controller = new AdminCardPageController();
		return controller.getDataPagePane();
	}
}