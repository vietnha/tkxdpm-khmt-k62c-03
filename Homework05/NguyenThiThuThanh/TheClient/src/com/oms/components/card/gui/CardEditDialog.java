package com.oms.components.card.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataEditDialog;

@SuppressWarnings("serial")
public class CardEditDialog extends ADataEditDialog<Card>{
	
	private JTextField nameField;
	private JTextField cardNumberField;
	private JTextField exprirationDateField;
	private JTextField moneyField;
	
	public CardEditDialog(Card card, IDataManageController<Card> controller) {
		super(card, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Họ và tên chủ thẻ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		
		row = getLastRowIndex();
		JLabel cardNumber = new JLabel("Số thẻ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(cardNumber, c);
		cardNumberField = new JTextField(15);
		cardNumberField.setText(t.getCardNumber());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(cardNumberField, c);

		row = getLastRowIndex();
		JLabel exprirationDateLabel = new JLabel("Ngày hết hạn");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(exprirationDateLabel, c);
		exprirationDateField = new JTextField(15);
		exprirationDateField.setText(t.getExprirationDate());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(exprirationDateField, c);
		
		row = getLastRowIndex();
		JLabel moneyLabel = new JLabel("Số dư");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(moneyLabel, c);
		moneyField = new JTextField(15);
		moneyField.setText(t.getMoney() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(moneyField, c);
		
		
	}

	@Override
	public Card getNewData() {
		t.setName(nameField.getText());
		t.setCardNumber(cardNumberField.getText());
		t.setExprirationDate(exprirationDateField.getText());
		t.setMoney(Integer.parseInt(moneyField.getText()));
		
		return t;
	}
}

