package com.oms.components.card.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.card.controller.AdminCardPageController;
import com.sun.xml.bind.v2.schemagen.xmlschema.List;

@SuppressWarnings("serial")
public class CardAddPane extends ADataAddPane<Card> {
	
	public CardAddPane(ADataPageController<Card> controller) {
		this.setController(controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		JButton addButton = new JButton("Thêm");
		add(addButton, c);
		
		IDataManageController<Card> manageController = new IDataManageController<Card>() {
			@Override
			public void update(Card t) {
			}

			@Override
			public void create(Card t) {
				if (controller instanceof AdminCardPageController) {
					((AdminCardPageController) controller).addCard(t);
					appendListPane(t);
				}
			}

			@Override
			public void read(Card t) {
			}

			@Override
			public void delete(Card t) {
				
			}
		};
		
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new CardAddDialog(manageController);
			}
		});	
	}
	
}

