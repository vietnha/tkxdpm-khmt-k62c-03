
package com.oms.components.card.gui;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.gui.ADataSearchPane;



@SuppressWarnings("serial")
public class CardSearchPane extends ADataSearchPane {	
	private JTextField nameField;
	private JTextField cardNumberField;
	private JTextField exprirationDateField;

	public CardSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Họ và tên chủ thẻ");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		JLabel cardNumberLabel = new JLabel("Số thẻ");
		cardNumberField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(cardNumberLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(cardNumberField, c);


		JLabel exprirationDateFieldLabel = new JLabel("Ngày hết hạn");
		exprirationDateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(exprirationDateFieldLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(exprirationDateField, c);
		

	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!nameField.getText().trim().equals("")) {
		res.put("name", nameField.getText().trim());
	    }
		if (!cardNumberField.getText().trim().equals("")) {
			res.put("cardNumber", cardNumberField.getText().trim());
		}	
		if (!exprirationDateField.getText().trim().equals("")) {
			res.put("exprirationDate", exprirationDateField.getText().trim());
		}	
		return res;
	}
}
