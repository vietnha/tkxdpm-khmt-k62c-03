
package com.oms.components.card.gui;
import javax.swing.JLabel;

import com.oms.bean.Card;
import com.oms.components.abstractdata.gui.ADataSinglePane;


@SuppressWarnings("serial")
public class CardSingelPane extends ADataSinglePane<Card>{
	private JLabel labelName;
	private JLabel labelCardNumber;
	private JLabel labelExprirationDate;
	private JLabel labelMoney;
		
	public CardSingelPane() {
		super();
	}
		
	
	public CardSingelPane(Card card) {
		this();
		this.t = card;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
	int row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelName = new JLabel();
	add(labelName, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelCardNumber = new JLabel();
	add(labelCardNumber, c);

	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelExprirationDate = new JLabel();
	add(labelExprirationDate, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelMoney = new JLabel();
	add(labelMoney, c);
	

	}
	
	
	@Override
	public void displayData() {
		labelName.setText("Họ và tên chủ thẻ: " + t.getName());
		labelCardNumber.setText("Số thẻ: " + t.getCardNumber());
		labelExprirationDate.setText("Ngày hết hạn: " + t.getExprirationDate());
		labelMoney.setText("Số dư: " + t.getMoney());
	}
}
