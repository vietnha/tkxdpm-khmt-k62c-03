package com.oms.components.card.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.Card;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.card.gui.CardSearchPane;
import com.oms.components.card.gui.CardSingelPane;
import com.oms.serverapi.CardApi;



public class AdminCardPageController extends AdminPPageController{
	@Override
	public List<? extends Card> search(Map<String, String> searchParams) {
		return new CardApi().getCard(searchParams);
	}
	
	@Override
	public CardSingelPane createSinglePane() {
		return new CardSingelPane();
	}
	
	@Override
	public CardSearchPane createSearchPane() {
		return new CardSearchPane();
	}
	
	@Override
	public Card updateCard(Card card) {
		return new CardApi().updateCard((Card) card);
	}

	public Card addCard(Card card) {
		return new CardApi().addCard((Card) card);
	}

	public Card deleteCard(Card card) {
		return new CardApi().deleteCard((Card) card);
	}

}
