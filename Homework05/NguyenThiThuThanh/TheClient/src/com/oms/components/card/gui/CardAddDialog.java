package com.oms.components.card.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataAddDialog;

@SuppressWarnings("serial")
public class CardAddDialog extends ADataAddDialog<Card> {

	private JTextField idField;
	private JTextField nameField;
	private JTextField cardNumberField;
	private JTextField exprirationDateField;
	private JTextField moneyField;

	public CardAddDialog(IDataManageController<Card> controller) {
		super(controller);
	}

	@Override
	public void buildControls() {
		t = new Card();
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Mã số");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		idField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(idField, c);

		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Họ và tên chủ thẻ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);

		row = getLastRowIndex();
		JLabel cardNumberLabel = new JLabel("Số thẻ");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(cardNumberLabel, c);
		cardNumberField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(cardNumberField, c);

		row = getLastRowIndex();
		JLabel exprirationDateLabel = new JLabel("Ngày hết hạn");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(exprirationDateLabel, c);
		exprirationDateField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(exprirationDateField, c);

		

		row = getLastRowIndex();
		JLabel moneyLabel = new JLabel("Số dư");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(moneyLabel, c);
		moneyField = new JTextField(15);

		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(moneyField, c);

	}

	@Override
	public Card getNewData() {
		t.setId(idField.getText());
		t.setName(nameField.getText());
		t.setCardNumber(cardNumberField.getText());
		t.setExprirationDate(exprirationDateField.getText());
		t.setMoney(Integer.parseInt(moneyField.getText()));
		return t;
	}
}
