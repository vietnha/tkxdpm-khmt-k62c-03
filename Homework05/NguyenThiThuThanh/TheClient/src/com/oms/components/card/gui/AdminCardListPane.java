package com.oms.components.card.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;

import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.card.controller.AdminCardPageController;
import com.oms.serverapi.CardApi;



@SuppressWarnings("serial")

public class AdminCardListPane extends ADataListPane<Card>{
	
	public AdminCardListPane(ADataPageController<Card> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Card> singlePane) {
		JButton editButton = new JButton("Chỉnh sửa");
		singlePane.addDataHandlingComponent(editButton);
		JButton deleteButton = new JButton("Xóa");
		singlePane.addDataHandlingComponent(deleteButton);
		
		IDataManageController<Card> manageController = new IDataManageController<Card>() {
			@Override
			public void update(Card t) {
				if (controller instanceof AdminCardPageController) {
					Card newCard = ((AdminCardPageController) controller).updateCard(t);
					singlePane.updateData(newCard);
				}
			}

			@Override
			public void create(Card t) {
			}

			@Override
			public void read(Card t) {
			}

			@Override
			public void delete(Card t) {
				if (controller instanceof AdminCardPageController) {
					((AdminCardPageController) controller).deleteCard(t);
					singlePane.setVisible(false);
				}
			}
		};
		
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new CardEditDialog(singlePane.getData(), manageController);
			}
		});	
		
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				manageController.delete(singlePane.getData());
			}
		});	
	}
}

