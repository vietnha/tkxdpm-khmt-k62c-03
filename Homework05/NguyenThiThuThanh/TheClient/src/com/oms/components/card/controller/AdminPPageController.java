package com.oms.components.card.controller;




import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.card.gui.AdminCardListPane;
import com.oms.components.card.gui.CardAddPane;


public abstract class AdminPPageController extends ADataPageController<Card> {
	public AdminPPageController() {
		super();
	}
	
	@Override
	public ADataListPane<Card> createListPane() {
		return new AdminCardListPane(this);
	}
	
	public abstract Card updateCard(Card card);
	
	@Override
	public ADataAddPane<Card> createAddPane() {
		return new CardAddPane(this);
	}
}
