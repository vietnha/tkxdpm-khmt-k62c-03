package com.oms.util.log;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class Logger {
	public static void printConsole(Object object) {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		try {
			String json = ow.writeValueAsString(object);
			System.out.println(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			System.out.println("Logger:: printConsole error");
			e.printStackTrace();
		}
	}
	public static void printObjectToJsonFile(String nameFile, Object object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(new File("log/"+nameFile+".json"), object);
		} catch (IOException e) {
			System.out.println("Logger:: printObjectToJsonFile error");
			e.printStackTrace();
		}
	}
}
