package com.oms.bean;
import java.util.Date;

public class BikeBack {
    private String id;

    private String name;

    private String typeId;

    private String cardId;

    private Date timeRental;
    private float cost;
    private int deposit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeId() {
        return typeId;
    }
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getCardId() {
        return cardId;
    }
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Date getTimeRental() {
        return timeRental;
    }
    public void setTimeRental(Date timeRental) {
        this.timeRental = timeRental;
    }

    public int getDeposit() {
        return deposit;
    }
    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public float getCost() {
        return cost;
    }
    public void setCost(float cost) {
        this.cost = cost;
    }
}

