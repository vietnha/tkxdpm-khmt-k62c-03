package com.oms.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.oms.bean.EBikeType;
import com.oms.bean.NormalBikeType;
import com.oms.bean.bike.Bike;
import com.oms.bean.bike.EBike;
import com.oms.bean.bike.EBikePayload;
import com.oms.bean.bike.NormalBike;
import com.oms.bean.bike.NormalBikePayload;
import com.oms.bean.BikeType;

public class BikeTypeApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public BikeTypeApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<BikeType> getAllBikeTypes() {
		WebTarget webTarget = client.target(PATH).path("biketypes");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<BikeType> res = response.readEntity(new GenericType<ArrayList<BikeType>>(){});
		System.out.println(res);
		return res;
	}

	public ArrayList<NormalBikeType> getNormalBikeTypes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("normalbiketype");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<NormalBikeType> res = response.readEntity(new GenericType<ArrayList<NormalBikeType>>() {});
		System.out.println(res);
		return res;
	}
	

	public NormalBikeType updateNormalBikeType(NormalBikeType normalbiketype) {
		WebTarget webTarget = client.target(PATH).path("normalbiketype").path(normalbiketype.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(normalbiketype, MediaType.APPLICATION_JSON));
		
		NormalBikeType res = response.readEntity(NormalBikeType.class);
		return res;
	}
	
	public NormalBikeType addNormalBikeType(NormalBikeType normalbiketype) {
				
		WebTarget webTarget = client.target(PATH).path("normalbiketype").path("add");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(normalbiketype, MediaType.APPLICATION_JSON));
		
		NormalBikeType res = response.readEntity(NormalBikeType.class);
		return res;

	}
	
	public NormalBikeType removeNormalBikeType(NormalBikeType normalbiketype) {
		
		WebTarget webTarget = client.target(PATH).path("normalbiketype").path("remove");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		
		Response response = invocationBuilder.post(Entity.entity(normalbiketype, MediaType.APPLICATION_JSON));
		
//		NormalBikeType res = response.readEntity(NormalBikeType.class);
		return null;
	}
	
	public ArrayList<EBikeType> getEBikeTypes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("ebiketype");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<EBikeType> res = response.readEntity(new GenericType<ArrayList<EBikeType>>() {});
		System.out.println(res);
		return res;
	}
	

	public EBikeType updateEBikeType(EBikeType ebiketype) {
		WebTarget webTarget = client.target(PATH).path("ebiketype").path(ebiketype.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(ebiketype, MediaType.APPLICATION_JSON));
		
		EBikeType res = response.readEntity(EBikeType.class);
		return res;
	}
	
	public EBikeType addEBikeType(EBikeType ebiketype) {
		
		WebTarget webTarget = client.target(PATH).path("ebiketype").path("add");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(ebiketype, MediaType.APPLICATION_JSON));
		
		EBikeType res = response.readEntity(EBikeType.class);
		return res;

	}
	
	public EBikeType removeEBikeType(EBikeType ebiketype) {
		
		WebTarget webTarget = client.target(PATH).path("ebiketype").path("remove");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		
		Response response = invocationBuilder.post(Entity.entity(ebiketype, MediaType.APPLICATION_JSON));
		
//		EBikeType res = response.readEntity(EBikeType.class);
		return null;
	}
	

}
