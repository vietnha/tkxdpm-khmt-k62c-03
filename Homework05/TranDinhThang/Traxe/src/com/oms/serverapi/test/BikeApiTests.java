package com.oms.serverapi.test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ EBikeApiTest.class })
public class BikeApiTests {

}