package com.oms.serverapi.test.rental;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.oms.bean.RentalData;
import com.oms.serverapi.RentalApi;

public class WhiteBoxGetRentalDataRentalApi {
	
//	public ArrayList<RentalData> GetRentalData(Map<String, String> queryParams) {
//		1  WebTarget webTarget = client.target(PATH).path("bike");
//		2  if (queryParams != null) {
//		3	 if (!queryParams.containsKey("id"))
//		4	 	return new ArrayList<RentalData>();
//		5	 for (String key : queryParams.keySet()) {
//		6		String value = queryParams.get(key);
//		7		webTarget = webTarget.queryParam(key, value);
//			}
//		}
//		8  Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
//		9  Response response = invocationBuilder.get();
//		10  ArrayList<RentalData> res = response.readEntity(new GenericType<ArrayList<RentalData>>() {});
//		11  System.out.println(res);
//		12 return res;
//	}
	
	private RentalApi api = new RentalApi();

	// White box testing all path
	
	/**
	 * (2) false: queryPrams = null
	 */
	@Test
	public void testGetRentalData1() {
		ArrayList<RentalData> list= api.getRentalData(null);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}
	
	/**
	 * (2) true, (3) true: queryPrams = {title="bike2"}
	 */
	@Test
	public void testGetRentalData2() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("title", "bike2");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 0);
	}
	
	/**
	 * (2) true, (3) false, (5) true: queryPrams = {id="bike2", title="hello"}
	 */
	@Test
	public void testGetRentalData3() {
		Map<String, String> queryPrams = new HashMap<String, String>();
		queryPrams.put("id", "bike2");
		queryPrams.put("title", "hello");
		ArrayList<RentalData> list= api.getRentalData(queryPrams);
		assertEquals("Error in getRentalData API!", list.size(), 1);
		assertEquals("Error in getRentalData API!", list.get(0).getId(), "bike2");
	}
	
}
