package com.oms.app.user;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class OMSUser extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public OMSUser(OMSUserController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);

		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);

		JPanel cardPage = controller.getCardPage();
		tabbedPane.addTab("Card", null, cardPage, "Card");

		JPanel bikePage = controller.getRentalPage();
		tabbedPane.addTab("Thuê xe", null, bikePage, "Thuê xe");
		
		JPanel bikeShowPage = controller.getBikePage();
		tabbedPane.addTab("Xem xe đạp thường", null, bikeShowPage, "NormalBike");
		
		JPanel ebikeShowPage = controller.getEBikePage();
		tabbedPane.addTab("Xem xe đạp điện", null, ebikeShowPage, "EBike");

		JPanel bikeBackPage = controller.getBikeBackPage();
		tabbedPane.addTab("Trả xe", null, bikeBackPage, "Trả xe");

//		tabbedPane.addTab("Compact Discs", null, new JPanel(), "Compact Discs");
//		tabbedPane.addTab("Digital Video Discs", null, new JPanel(), "Digital Video Discs");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Ứng dụng thuê xe đạp");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new OMSUser(new OMSUserController());
			}
		});
	}
}