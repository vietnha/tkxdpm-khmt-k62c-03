package com.oms.app.admin;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.oms.components.bike.BikeState;

@SuppressWarnings("serial")
public class OMSAdmin extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;

	public OMSAdmin(OMSAdminController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);

		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);		
		
		JPanel parkingPage = controller.getParkingPage();
		tabbedPane.addTab("Parking", null, parkingPage, "Parking");
		
		
		JPanel bikePage = controller.getBikePage();
		tabbedPane.addTab("Normal Bike", null, bikePage, "NormalBike");
		JPanel ebikePage = controller.getEBikePage();
		tabbedPane.addTab("Electric Bike", null, ebikePage, "EBike");
		
		JPanel normalBikeTypePage = controller.getNormalBikeTypePage();
		tabbedPane.addTab("NormalBike Type", null, normalBikeTypePage, "NormalBike Type");
		JPanel eBikeTypePage = controller.getEBikeTypePage();
		tabbedPane.addTab("EBike Type", null, eBikeTypePage, "EBike Type");
		
		
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				switch (tabbedPane.getSelectedIndex()) {
					case 1:
						BikeState.getInstance().setStateTab(BikeState.NORMALBIKE);
						break;
					case 2:
						BikeState.getInstance().setStateTab(BikeState.EBIKE);
						break;
				}
			}
	    });

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Quản lý EcoBikeRental");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new OMSAdmin(new OMSAdminController());
			}
		});
	}
}