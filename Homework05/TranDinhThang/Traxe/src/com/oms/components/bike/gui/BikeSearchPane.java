package com.oms.components.bike.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.gui.ADataSearchPane;
import com.oms.util.JTextFieldUtil;

@SuppressWarnings("serial")
public class BikeSearchPane extends ADataSearchPane {
//	private JTextField typeId;
	private JTextField parkingId;
	private JTextField name;
	private JTextField licensePlate;
	private JTextField producer;
	private JTextField nameType;
	private JTextField so_yen;
//	private JTextField so_ban_dap;
	private JTextField so_ghe_sau;

	public BikeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
//		typeId = addSlot("typeId");
		parkingId = addSlot("Parking ID");
		name = addSlot("Name");
		licensePlate = addSlot("License Plate");
		producer = addSlot("Producer");
		nameType = addSlot("Type Name");
		so_yen = addSlot("Count Saddles");
//		so_ban_dap = addSlot("Count Pendals");
		so_ghe_sau = addSlot("Count Seats");
	}
	
	public JTextField addSlot(String labelTitle) {
		JLabel label = new JLabel(labelTitle);
		JTextField field = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(label, c);
		c.gridx = 1;
		c.gridy = row;
		add(field, c);
		return field;
	}

	public void putParam(Map<String, String> res, String key, JTextField field) {
		if (field.getText() == null || field.getText().equals("")) {
			res.put(key, "");
		}
		if (!field.getText().trim().equals("")) {
			res.put(key, field.getText().trim());
		}
	}
	
	public void putParamInt(Map<String, String> res, String key, JTextField field) {
		if (field.getText() == null || field.getText().equals("")) {
			res.put(key, "");
		}
		if (!field.getText().trim().equals("")) {
//			res.put(key, field.getText().trim());
			res.put(key, Integer.toString(JTextFieldUtil.getIntegerIfSuccess(field, 0)));
		}
	}
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
//		putParam(res, "typeId", typeId);
		putParam(res, "parkingId", parkingId);
		putParam(res, "name", name);
		putParam(res, "licensePlate", licensePlate);
		putParam(res, "producer", producer);
		putParam(res, "nameType", nameType);
		putParamInt(res, "so_yen", so_yen);
//		putParam(res, "so_ban_dap", so_ban_dap);
		putParamInt(res, "so_ghe_sau", so_ghe_sau);

		return res;
	}
}