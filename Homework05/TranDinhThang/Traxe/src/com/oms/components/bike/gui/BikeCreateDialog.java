package com.oms.components.bike.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.bike.Bike;
import com.oms.components.abstractdata.controller.IDataCreateController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.abstractdata.gui.ADataCreateDialog;
import com.oms.util.JTextFieldUtil;

@SuppressWarnings("serial")
public class BikeCreateDialog extends ADataCreateDialog<Bike>{
	
	private JTextField id;
	private JTextField typeId;
	private JTextField parkingId;
	private JTextField name;
	private JTextField weight;
	private JTextField licensePlate;
	private JTextField manuafacturingDate;
	private JTextField cost;
	private JTextField producer;
	
	public BikeCreateDialog(Bike bike, IDataManageController<Bike> controller) {
		super(bike, controller);
	}

	@Override
	public void buildControls() {
		id = addSlot("ID", t.getId());
		typeId = addSlot("Type ID", t.getTypeId());
		parkingId =addSlot("Parking ID", t.getParkingId());
		name = addSlot("Name", t.getName());
		weight = addSlot("Weight", Float.toString(t.getWeight()));
		licensePlate = addSlot("License Plate", t.getLicensePlate());
		manuafacturingDate = addSlot("Manuafacturing Date", t.getManuafacturingDate());
		cost = addSlot("Cost", Float.toString(t.getCost()));
		producer = addSlot("Producer", t.getProducer());
	}

	public JTextField addSlot(String labelTitle, String text) {
		int row = getLastRowIndex();
		JLabel label = new JLabel(labelTitle);
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(label, c);
		JTextField field = new JTextField(15);
		field.setText(text);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(field, c);
		return field;
	}
	
	@Override
	public Bike getNewData() {
		String w;
		w = id.getText().trim();
		if (!w.equals("")) t.setId(w);
		w = typeId.getText().trim();
		if (!w.equals("")) t.setTypeId(w);
		w = parkingId.getText().trim();
		if (!w.equals("")) t.setParkingId(w);
		w = name.getText().trim();
		if (!w.equals("")) t.setName(w);
		t.setWeight(JTextFieldUtil.getFloatIfSuccess(weight, t.getWeight()));
		w = licensePlate.getText().trim();
		if (!w.equals("")) t.setLicensePlate(w);
		w = manuafacturingDate.getText().trim();
		if (!w.equals("")) t.setManuafacturingDate(w);
		t.setCost(JTextFieldUtil.getFloatIfSuccess(cost, t.getCost()));
		w = producer.getText().trim();
		if (!w.equals("")) t.setProducer(w);
		
		return t;
	}
}

