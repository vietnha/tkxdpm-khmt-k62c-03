package com.oms.components.biketype.ebiketype.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.biketype.gui.BikeTypeSearchPane;

@SuppressWarnings("serial")
public class EBikeTypeSearchPane extends BikeTypeSearchPane {
	public EBikeTypeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
	}
	
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();		
		return res;
	}
}
