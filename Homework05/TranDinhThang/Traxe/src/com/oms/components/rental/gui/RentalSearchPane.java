
package com.oms.components.rental.gui;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.gui.ADataSearchPane;



@SuppressWarnings("serial")
public class RentalSearchPane extends ADataSearchPane {	
	private JTextField bikeIdField;

	public RentalSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel bikeIdLabel = new JLabel("Mã xe");
		bikeIdField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(bikeIdLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(bikeIdField, c);
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!bikeIdField.getText().trim().equals("")) {
		res.put("id", bikeIdField.getText().trim());
	    }
		return res;
	}
}
