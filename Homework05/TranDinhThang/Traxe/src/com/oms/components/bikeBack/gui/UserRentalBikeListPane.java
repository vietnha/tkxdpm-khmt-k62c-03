package com.oms.components.bikeBack.gui;

import com.oms.bean.BikeBack;
import com.oms.bean.Card;
import com.oms.bean.Parking;
import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.serverapi.CardViewApi;
import com.oms.serverapi.ParkingApi;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")

public class UserRentalBikeListPane extends ADataListPane<BikeBack> {

	public UserRentalBikeListPane(ADataPageController<BikeBack> controller) {
		this.controller = controller;
	}

	@Override
	public void decorateSinglePane(ADataSinglePane<BikeBack> singlePane) {
		JButton button = new JButton("Trả xe");
		singlePane.addDataHandlingComponent(button);

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Map<String, String> searchParams = new HashMap<String, String>();

				searchParams.put("id", "");

				List<Parking> list = new ParkingApi().getParking(searchParams);
				new ParkingListDialog(list, singlePane.getData());
			}
		});
	}
}
