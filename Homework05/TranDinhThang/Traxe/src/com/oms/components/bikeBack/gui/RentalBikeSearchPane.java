
package com.oms.components.bikeBack.gui;

import com.oms.components.abstractdata.gui.ADataSearchPane;

import javax.swing.*;
import java.util.Map;


@SuppressWarnings("serial")
public class RentalBikeSearchPane extends ADataSearchPane {
	private JTextField bikeIdField;

	public RentalBikeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel bikeIdLabel = new JLabel("Nhập mã xe đang thuê");
		bikeIdField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(bikeIdLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(bikeIdField, c);

	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!bikeIdField.getText().trim().equals("")) {
		res.put("id", bikeIdField.getText().trim());
	    }
		return res;
	}
}
