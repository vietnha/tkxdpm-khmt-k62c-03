package com.oms.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oms.bean.BikeType;
import com.oms.bean.Card;
import com.oms.bean.Parking;
import com.oms.bean.bike.Bike;


public class Seed {
//	private ArrayList<Media> medias;
	
	private ArrayList<Parking> parking;
	private ArrayList<BikeType> biketypes;
	private ArrayList<Card> cards;
	private Map<String, Map<Date,Bike>> rentalDatas;

	
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		parking = new ArrayList<Parking>();
		parking.addAll(generateDataFromFile( new File(getClass().getResource("./parking.json").getPath()).toString()));
		
		biketypes = new ArrayList<BikeType>();
		biketypes.addAll(generateDataFromFile1( new File(getClass().getResource("./normalbike.json").getPath()).toString()));
		biketypes.addAll(generateDataFromFile1( new File(getClass().getResource("./ebike.json").getPath()).toString()));
		cards = new ArrayList<Card>();
		
		cards.addAll(generateDataFromFile2( new File(getClass().getResource("./card.json").getPath()).toString()));
		
		rentalDatas = new HashMap<String, Map<Date,Bike>>();
	}

	public Map<String, Map<Date, Bike>> getRentalDatas() {
		return rentalDatas;
	}

	private ArrayList<? extends Parking> generateDataFromFile(String filePath){
		ArrayList<? extends Parking> res = new ArrayList<Parking>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Parking>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	public ArrayList<Parking> getParking() {
		return parking;
	}
	
	private ArrayList<? extends BikeType> generateDataFromFile1(String filePath){
		ArrayList<? extends BikeType> res = new ArrayList<BikeType>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<BikeType>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}

	
	private ArrayList<? extends Card> generateDataFromFile2(String filePath){
		ArrayList<? extends Card> res = new ArrayList<Card>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Card>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		return res;
	}
	
	public ArrayList<BikeType> getBikeTypes() {
		return biketypes;
	}


	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}
	
	public ArrayList<Card> getCards() {
		return cards;
	}
}
