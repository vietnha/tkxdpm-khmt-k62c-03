package com.oms.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.oms.util.log.Logger;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("bike")
@JsonSubTypes({ @Type(value = NormalBikeType.class, name = "normalbike"), @Type(value = EBikeType.class, name = "ebike")})
public class BikeType {
	private String id;
	
	private String name;

	private int so_yen;

	private int tien_coc;
	private int so_ghe_sau;
	private float muc_gia;
	
	public BikeType() {
		super();
	}

	public BikeType(String id, String name, int so_yen, int so_ghe_sau, float muc_gia, int tien_coc) {
		super();
		this.id = id;
		this.name = name;
		this.so_yen = so_yen;
		this.tien_coc = tien_coc;
		this.so_ghe_sau = so_ghe_sau;
		this.muc_gia = muc_gia;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSo_yen() {
		return so_yen;
	}

	public void setSo_yen(int so_yen) {
		this.so_yen = so_yen;
	}

	public int getTien_coc() {
		return tien_coc;
	}

	public void setTien_coc(int tien_coc) {
		this.tien_coc = tien_coc;
	}
	
	public int getSo_ghe_sau() {
		return so_ghe_sau;
	}

	public void setSo_ghe_sau(int so_ghe_sau) {
		this.so_ghe_sau = so_ghe_sau;
	}
	
	public float getMuc_gia() {
		return muc_gia;
	}

	public void setMuc_gia(float muc_gia) {
		this.muc_gia = muc_gia;
	}
	
	@Override
	public String toString() {
		return "id: " + this.id + ", name: " + this.name + ", so yen: " + this.so_yen + ", tien coc: " + this.tien_coc +", so ghe sau: " + this.so_ghe_sau + ", muc gia: " + this.muc_gia;
	}
	
	
	public boolean match(BikeType bike) {
		if (bike == null)
			return true;
		
		
		if (bike.id != null && !bike.id.equals("") && !this.id.contains(bike.id)) {
			return false;
		}
		if (bike.name != null && !bike.name.equals("") && !this.name.contains(bike.name)) {
			return false;
		}
		if (bike.so_yen != 0 && this.so_yen != bike.so_yen) {
			return false;
		}
		if (bike.tien_coc != 0 && this.tien_coc != bike.tien_coc) {
			return false;
		}
		if (bike.so_ghe_sau != 0 && this.so_ghe_sau != bike.so_ghe_sau) {
			return false;
		}
		if (bike.muc_gia != 0 && this.muc_gia != bike.muc_gia) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BikeType) {
			return this.id.equals(((BikeType) obj).id);
		}
		return false;
	}
}