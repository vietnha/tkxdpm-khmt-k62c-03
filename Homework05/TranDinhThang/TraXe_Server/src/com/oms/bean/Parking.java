package com.oms.bean;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("parking")
@JsonSubTypes({ @Type(value = Parking.class, name = "parking") })
public class Parking {
	private String id;
	private String name;
	private String address;
	private int numberOfBikes;
	private int numberOfeBikes;
	private int numberOfTwinBikes;
	private int numberOfEmtyDocks;
	private float distance;
	private float time;
	
	public Parking() {
		super();
	}
	public Parking(String id,String name,String address,int numberOfBikes,
			int numberOfeBikes,int numberOfTwinBikes,int numberOfEmtyDocks,float distance,float time
			) {
		this.id = id;
		this.name=name;
		this.address=address;
		this.numberOfBikes=numberOfBikes;
		this.numberOfeBikes=numberOfeBikes;
		this.numberOfTwinBikes=numberOfTwinBikes;
		this.numberOfEmtyDocks=numberOfEmtyDocks;
		this.distance=distance;
		this.time=time;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public int getNumberOfBikes() {
		return numberOfBikes;
	}

	public void setNumberOfBikes(int numberOfBikes) {
		this.numberOfBikes = numberOfBikes;
	}
	
	public int getNumberOfTwinBikes() {
		return numberOfTwinBikes;
	}

	public void setNumberOfTwinBikes(int numberOfTwinBikes) {
		this.numberOfTwinBikes = numberOfTwinBikes;
	}
	public int getNumberOfEmtyDocks() {
		return numberOfEmtyDocks;
	}

	public void setNumberOfEmtyDocks(int numberOfEmtyDocks) {
		this.numberOfEmtyDocks = numberOfEmtyDocks;
	}
	public int getNumberOfeBikes() {
		return numberOfeBikes;
	}

	public void setNumberOfeBikes(int numberOfeBikes) {
		this.numberOfeBikes = numberOfeBikes;
	}
	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}
	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "id: " + this.id + ", name: " + this.name + 
				", address: " + this.address+ ", numberOfBikes: " + this.numberOfBikes+", numberOfeBikes: " + this.numberOfeBikes+", numberOfTwinBikes: " + this.numberOfTwinBikes
				+", numberOfEmtyDocks: " + this.numberOfEmtyDocks + ", distance: " + this.distance + ", time: " + this.time ;
	}
	public boolean match(Parking parking) {
		if (parking == null)
			return true;
		
		
		if (parking.id != null && !parking.id.equals("") && !this.id.contains(parking.id)) {
			return false;
		}
		if (parking.name != null && !parking.name.equals("") && !this.name.contains(parking.name)) {
			return false;
		}
		if (parking.address != null && !parking.address.equals("") && !this.address.contains(parking.address)) {
			return false;
		}
		if (parking.numberOfBikes != 0 && this.numberOfBikes != parking.numberOfBikes) {
			return false;
		}
		if (parking.numberOfeBikes != 0 && this.numberOfeBikes != parking.numberOfeBikes) {
			return false;
		}
		if (parking.numberOfEmtyDocks != 0 && this.numberOfEmtyDocks != parking.numberOfEmtyDocks) {
			return false;
		}
		if (parking.numberOfTwinBikes != 0 && this.numberOfTwinBikes != parking.numberOfTwinBikes) {
			return false;
		}
		if (parking.distance != 0 && this.distance != parking.distance) {
			return false;
		}
		if (parking.time != 0 && this.time != parking.time) {
			return false;
		}
		
		return true;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Parking) {
			return this.id.equals(((Parking) obj).id);
		}
		return false;
	}
}