package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.Parking;

public interface IMediaDatabase {
	public ArrayList<Parking> searchParking(Parking parking);
	public Parking updateParking(Parking parking);
	public Parking addParking(Parking parking);
	public Parking deleteParking(Parking parking);

}
