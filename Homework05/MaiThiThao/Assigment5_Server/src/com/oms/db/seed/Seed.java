package com.oms.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oms.bean.Parking;

public class Seed {
//	private ArrayList<Media> medias;
	
	private ArrayList<Parking> parking;
	
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		parking = new ArrayList<Parking>();
		parking.addAll(generateDataFromFile( new File(getClass().getResource("./parking.json").getPath()).toString()));
		
	}
	
	private ArrayList<? extends Parking> generateDataFromFile(String filePath){
		ArrayList<? extends Parking> res = new ArrayList<Parking>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Parking>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	public ArrayList<Parking> getParking() {
		return parking;
	}


	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}
}
