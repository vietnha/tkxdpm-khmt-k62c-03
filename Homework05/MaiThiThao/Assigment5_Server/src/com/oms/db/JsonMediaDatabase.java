package com.oms.db;

import java.util.ArrayList;
import com.oms.bean.Parking;
import com.oms.db.seed.Seed;

public class JsonMediaDatabase implements IMediaDatabase{
	private static IMediaDatabase singleton = new JsonMediaDatabase();
	private ArrayList<Parking> parkings = Seed.singleton().getParking();
	
	private JsonMediaDatabase() {
	}
	
	public static IMediaDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Parking> searchParking(Parking parking) {
		ArrayList<Parking> res = new ArrayList<Parking>();
		for (Parking b: parkings) {
			if (b.match(parking)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public Parking addParking(Parking parking) {
		for (Parking m: parkings) {
			if (m.equals(parking)) {
				return null;
			}
		}
		
		parkings.add(parking);
		return parking;
	}
	
	@Override
	public Parking updateParking(Parking parking) {
		for (Parking m: parkings) {
			if (m.equals(parking)) {
				parkings.remove(m);
				parkings.add(parking);
				return parking;
			}
		}
		return null;
	}

	@Override
	public Parking deleteParking(Parking parking) {
		for (Parking m: parkings) {
			if (m.equals(parking)) {
				parkings.remove(m);
				return parking;
			}
		}
		return null;
	}
}
