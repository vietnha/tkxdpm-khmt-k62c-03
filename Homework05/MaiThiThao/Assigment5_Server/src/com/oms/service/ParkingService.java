package com.oms.service;
import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.Parking;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;

@Path("/parking")
public class ParkingService {

	private IMediaDatabase mediaDatabase;

	public ParkingService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Parking> getParking(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("address") String address, @QueryParam("distance") float distance,
			@QueryParam("time") float time
			) {
		Parking parking = new Parking(id, name,address, 0, 0, 0, 0, distance, time);
		parking.setName(name);
		parking.setAddress(address);
		parking.setDistance(distance);
		parking.setTime(time);
		ArrayList<Parking> res = mediaDatabase.searchParking(parking);
		return res;
	}
	

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Parking updateParking(@PathParam("id") String id, Parking parking) {
		return mediaDatabase.updateParking(parking);
	}
	//
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Parking addParking(Parking parking) {
		Parking res = mediaDatabase.addParking(parking);
		return res;
	}
	
	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Parking deleteParking(Parking parking) {
		Parking res = mediaDatabase.deleteParking(parking);
		return res;
	}
}