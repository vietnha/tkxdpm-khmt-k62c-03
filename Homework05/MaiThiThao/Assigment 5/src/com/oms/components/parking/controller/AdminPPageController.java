package com.oms.components.parking.controller;




import com.oms.bean.Parking;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataAddPane;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.parking.gui.AdminParkingListPane;
import com.oms.components.parking.gui.ParkingAddPane;


public abstract class AdminPPageController extends ADataPageController<Parking> {
	public AdminPPageController() {
		super();
	}
	
	@Override
	public ADataListPane<Parking> createListPane() {
		return new AdminParkingListPane(this);
	}
	
	public abstract Parking updateParking(Parking parking);
	
	@Override
	public ADataAddPane<Parking> createAddPane() {
		return new ParkingAddPane(this);
	}
}
