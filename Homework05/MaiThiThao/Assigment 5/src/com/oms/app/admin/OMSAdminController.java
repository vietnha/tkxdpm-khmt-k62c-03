package com.oms.app.admin;

import javax.swing.JPanel;
import com.oms.bean.Parking;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.parking.controller.AdminParkingPageController;



public class OMSAdminController {
	public OMSAdminController() {
	}
	
	public JPanel getParkingPage() {
		ADataPageController<Parking> controller = new AdminParkingPageController();
		return controller.getDataPagePane();
	}
}