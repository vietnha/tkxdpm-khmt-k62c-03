package com.oms.serverapi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.oms.bean.Parking;
import com.oms.serverapi.ParkingApi;


public class ParkingApiTest {
	private ParkingApi api = new ParkingApi();
	@Test
	public void testGetAllParking() {
		ArrayList<Parking> list= api.getParking(null);
		assertEquals("Error in getParking API!", list.size(), 6);
	}
	
	@Test(timeout = 1000)
	public void testResponse() {
		api.getParking(null);
	}
	
	@Test
	public void testUpdateParking() {
		ArrayList<Parking> list= api.getParking(null);
		assertTrue("No data", list.size() > 0);
		Parking parking = list.get(0);
		String newName= "O'Reilly";
		parking.setName(newName);
		api.updateParking(parking);
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("Name", newName);
		list = api.getParking(params);
		assertTrue("Eror in updateBook API!", list.size() > 0);
		Parking newParking = api.getParking(params).get(0);
		assertEquals("Eror in updateParking API!", newParking.getName(), newName);
	}

}
