package com.oms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.oms.bean.Card;
import com.oms.bean.bike.Bike;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;
import com.oms.db.seed.Seed;

@Path("/card")
public class CardService {

	private IMediaDatabase mediaDatabase;

	public CardService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Card> getCard(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("securityCode") String securityCode, @QueryParam("exprirationDate") String exprirationDate) {
		Card card = new Card(id, name, securityCode, exprirationDate, 0);
		card.setName(name);
		card.setSecurityCode(securityCode);
		card.setExprirationDate(exprirationDate);
		ArrayList<Card> res = mediaDatabase.searchCard(card);
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Card updateCard(@PathParam("id") String id, Card card) {
		return mediaDatabase.updateCard(card);
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Card addCard(Card card) {
		Card res = mediaDatabase.addCard(card);
		return res;
	}
	
	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Card deleteCard(Card card) {
		Card res = mediaDatabase.deleteCard(card);
		return res;
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Card> getCardsNoAgs() {
		Card card = new Card("", "", "","",0);
		ArrayList<Card> res = mediaDatabase.searchCard(card);
		Map<String, Map<Date, Bike>> map = Seed.singleton().getRentalDatas();
		ArrayList<Card> resRemove = new ArrayList<Card>();
		for (Card c: res) {
			if (map.containsKey(c.getId())) {
				resRemove.add(c);
			}
		}
		res.removeAll(resRemove);
		return res;
	}
}