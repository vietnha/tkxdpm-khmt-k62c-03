package com.oms.service;

import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.oms.bean.BikeBack;
import com.oms.bean.BikeType;
import com.oms.bean.RentalData;
import com.oms.bean.bike.Bike;
import com.oms.bean.bike.NormalBike;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;
import com.oms.db.bike.helper.JsonBikeInListHelperDatabase;
import com.oms.db.seed.Seed;
import com.oms.db.seed.bike.BikesData;

@Path("/bikeBack")
public class BikeBackService {

    private IMediaDatabase mediaDatabase;

    public BikeBackService() {
        mediaDatabase = JsonMediaDatabase.singleton();
    }

    @GET
    @Path("/rentalDatas")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<BikeBack> getRantalDatas(@PathParam("id") String id) {
        ArrayList<BikeBack> bikeBacks = new ArrayList<BikeBack>();
        Map<String, Map<Date, Bike>> rs = Seed.singleton().getRentalDatas();
        Set<String> set = rs.keySet();
        for (String card : set) {
            Map<Date, Bike> dateBikeMap = rs.get(card);
            Set<Date> set2 = dateBikeMap.keySet();
            for (Date date : set2) {
                Bike bike = dateBikeMap.get(date);
                System.out.println(id);
//				if (bike.getId().equals(id)) {
                BikeBack bikeBack = new BikeBack();
                bikeBack.setId(bike.getId());
                bikeBack.setName(bike.getName());
                bikeBack.setTypeId(bike.getTypeId());
                bikeBack.setCardId(card);
                bikeBack.setTimeRental(date);
                bikeBack.setCost(bike.getCost());
                List<BikeType> bts = Seed.singleton().getBikeTypes();
                for (BikeType bt: bts) {
                    if (bike.getTypeId().equals(bt.getId())) {
                        bikeBack.setDeposit(bt.getTien_coc());
                        break;
                    }
                }
                bikeBacks.add(bikeBack);
//				}
            }
        }
        return bikeBacks;
    }


    @POST
    @Path("/{id}/{cardId}/{parkingId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void bikeBackApi(@PathParam("id") String id, @PathParam("cardId") String cardId, @PathParam("parkingId") String parkingId) {
        ArrayList<Bike> bikes = BikesData.singleton().getAllBikes();
        for (Bike b: bikes) {
            if (b.getId().equals(id)) {
                b.setParkingId("");
                Map<String, Map<Date, Bike>> map = Seed.singleton().getRentalDatas();
                // delete thue xe
                map.remove(cardId);
                // update Parking
                JsonBikeInListHelperDatabase.singleton().updateParkingIdById(id, parkingId);
                break;
            }
        }
    }
}