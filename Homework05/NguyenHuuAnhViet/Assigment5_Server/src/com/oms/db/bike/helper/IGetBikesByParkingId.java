package com.oms.db.bike.helper;

import java.util.ArrayList;

import com.oms.bean.bike.Bike;

public interface IGetBikesByParkingId {
	public ArrayList<Bike> getBikesByParkingId(String parkingId);
}
