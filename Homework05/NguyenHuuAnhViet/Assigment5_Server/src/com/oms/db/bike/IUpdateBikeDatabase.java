package com.oms.db.bike;

import com.oms.bean.bike.Bike;
import com.oms.playload.bike.BikePayload;

public interface IUpdateBikeDatabase {
	public BikePayload updateBike(Bike obj);
}
