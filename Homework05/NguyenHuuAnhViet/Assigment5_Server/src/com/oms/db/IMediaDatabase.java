package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.BikeType;
import com.oms.bean.Card;
import com.oms.bean.Parking;
import com.oms.bean.RentalData;

public interface IMediaDatabase {
	public ArrayList<Parking> searchParking(Parking parking);
	public Parking updateParking(Parking parking);
	public Parking addParking(Parking parking);
	public Parking removeParking(Parking parking);
	
	public ArrayList<BikeType> searchBikeType(BikeType biketype);
	public BikeType updateBikeType(BikeType biketype);
	public BikeType addBikeType(BikeType biketype);
	public BikeType removeBikeType(BikeType biketype);

	public BikeType getInforFromID(String id);
	public boolean matchBikeType(BikeType biketype);
	public ArrayList<RentalData> searchBikeById(RentalData rentalData);
	
	public ArrayList<Card> searchCard(Card card);
	public Card updateCard(Card card);
	public Card addCard(Card card);
	public Card deleteCard(Card card);
}
