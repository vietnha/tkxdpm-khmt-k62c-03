package com.oms.util;

public class StringUtil {
	public static Float getFloat(String s) {
		Float f = null;
		try {
			f = Float.parseFloat(s);
		}
		catch (Exception e){
			System.out.print("Convert ``" + s + "`` to float error!!");
			return null;
		}
		if (Float.isFinite(f)) return f;
		System.out.print("Convert ``" + s + "`` to float error!!");
		return null;
	}
	public static Integer getInt(String s) {
		Integer i = null;
		try {
			i = Integer.parseInt(s);
		}
		catch (Exception e){
			System.out.print("Convert ``" + s + "`` to int error!!");
			return null;
		}
		return i;
	}
	public static void main(String []args) {
//		System.out.print(StringUtil.getFloat("   ").toString());
//		System.out.print(StringUtil.getInt(" \\		\\ss").toString());
	}
}
