package com.oms.components.parking.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.Parking;
import com.oms.components.parking.gui.ParkingSearchPane;
import com.oms.components.parking.gui.ParkingSingelPane;
import com.oms.serverapi.ParkingApi;



public class AdminParkingPageController extends AdminPPageController{
	@Override
	public List<? extends Parking> search(Map<String, String> searchParams) {
		return new ParkingApi().getParking(searchParams);
	}
	
	@Override
	public ParkingSingelPane createSinglePane() {
		return new ParkingSingelPane();
	}
	
	@Override
	public ParkingSearchPane createSearchPane() {
		return new ParkingSearchPane();
	}
	
	@Override
	public Parking updateParking(Parking parking) {
		return new ParkingApi().updateParking((Parking) parking);
	}
	
	public Parking addParking(Parking parking) {
		return new ParkingApi().addParking((Parking) parking);
	}

	public Parking removeParking(Parking parking) {
		return new ParkingApi().removeParking((Parking) parking);
	}

	
	
}
