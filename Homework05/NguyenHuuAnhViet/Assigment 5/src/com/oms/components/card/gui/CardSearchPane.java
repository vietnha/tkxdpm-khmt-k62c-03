
package com.oms.components.card.gui;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.components.abstractdata.gui.ADataSearchPane;



@SuppressWarnings("serial")
public class CardSearchPane extends ADataSearchPane {	
	private JTextField nameField;
	private JTextField securityCodeField;
	private JTextField exprirationDateField;

	public CardSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Tên chủ thẻ");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		JLabel securityCodeLabel = new JLabel("Mã bảo mật");
		securityCodeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(securityCodeLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(securityCodeField, c);


		JLabel exprirationDateFieldLabel = new JLabel("Ngày hết hạn");
		exprirationDateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(exprirationDateFieldLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(exprirationDateField, c);
		

	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!nameField.getText().trim().equals("")) {
		res.put("name", nameField.getText().trim());
	    }
		if (!securityCodeField.getText().trim().equals("")) {
			res.put("securityCode", securityCodeField.getText().trim());
		}	
		if (!exprirationDateField.getText().trim().equals("")) {
			res.put("exprirationDate", exprirationDateField.getText().trim());
		}	
		return res;
	}
}
