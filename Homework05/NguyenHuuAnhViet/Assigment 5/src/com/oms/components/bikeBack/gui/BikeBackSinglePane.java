
package com.oms.components.bikeBack.gui;

import com.oms.bean.BikeBack;
import com.oms.bean.RentalData;
import com.oms.components.abstractdata.gui.ADataSinglePane;

import javax.swing.*;


@SuppressWarnings("serial")
public class BikeBackSinglePane extends ADataSinglePane<BikeBack>{
	private JLabel labelId;
	private JLabel labelName;
	private JLabel labelTypeId;
	private JLabel labelCardId;
	private JLabel labelTimeRental;
	private JLabel labelCost;
	private JLabel labelDeposit;
	
	public BikeBackSinglePane() {
		super();
	}
		
	
	public BikeBackSinglePane(BikeBack bikeBack) {
		this();
		this.t = bikeBack;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();

		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelId = new JLabel();
		add(labelId, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelTypeId = new JLabel();
		add(labelTypeId, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCardId = new JLabel();
		add(labelCardId, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelTimeRental = new JLabel();
		add(labelTimeRental, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelDeposit = new JLabel();
		add(labelDeposit, c);
	}
	
	
	
	
	@Override
	public void displayData() {
		labelId.setText("Id of bike: " + t.getId());
		labelName.setText("Name of bike: " + t.getName());
		labelTypeId.setText("Type of bike: " + t.getTypeId());
		labelCardId.setText("Card of rentalBike : " + t.getCardId());
		labelTimeRental.setText("Time of rentalBike : " + t.getTimeRental());
		labelCost.setText("Cost of rentalBike : " + t.getCost());
		labelDeposit.setText("Deposit of rentalBike : " + t.getDeposit());
	}
}
