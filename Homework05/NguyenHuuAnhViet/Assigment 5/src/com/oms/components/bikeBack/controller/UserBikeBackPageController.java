package com.oms.components.bikeBack.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.BikeBack;
import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.bikeBack.gui.BikeBackSinglePane;
import com.oms.components.bikeBack.gui.RentalBikeSearchPane;
import com.oms.components.bikeBack.gui.UserRentalBikeListPane;
import com.oms.components.rental.gui.BikeSingelPane;
import com.oms.serverapi.BikeBackApi;
import com.oms.serverapi.RentalApi;



public class UserBikeBackPageController extends ADataPageController<BikeBack> {
	public UserBikeBackPageController() {
		super();
	}

	@Override
	public List<? extends BikeBack> search(Map<String, String> searchParams) {
		return new BikeBackApi().getRentalDatas(searchParams);
	}

	@Override
	public BikeBackSinglePane createSinglePane() {
		return new BikeBackSinglePane();
	}

	@Override
	public RentalBikeSearchPane createSearchPane() {
		return new RentalBikeSearchPane();
	}

	@Override
	public ADataListPane<BikeBack> createListPane() {
		return new UserRentalBikeListPane(this);
	}

}
