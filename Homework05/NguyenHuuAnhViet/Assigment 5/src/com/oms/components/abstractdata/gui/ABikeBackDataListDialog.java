package com.oms.components.abstractdata.gui;

import com.oms.bean.BikeBack;
import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.BikeBackController;
import com.oms.components.abstractdata.controller.RentalController;
import com.oms.serverapi.BikeBackApi;
import com.oms.serverapi.CardBankApi;
import com.oms.serverapi.RentalApi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public abstract class ABikeBackDataListDialog<T> extends JDialog {
	protected List<T> t;
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();
	private BikeBack bikeBack;
	private BikeBackController bikeBackController;

	List<JButton> listButton = new ArrayList<JButton>();

	public ABikeBackDataListDialog(List<T> t, BikeBack bikeBack) {
		super((Frame) null, "Chọn bãi xe", true);
		this.t = t;
		this.bikeBack = bikeBack;
		this.setBikeBackController(new BikeBackController() {
			@Override
			public void bikeBackIndex(int i) {
				new CardBankApi().pay(bikeBack.getCardId(), (int) -bikeBack.getCost());
				new BikeBackApi().bikeBackApi(bikeBack, getParkingId(i));
			}

		});
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		for (int i = 0; i < t.size(); i++) {
			this.buildControls(i);
			JButton saveButton = new JButton("Trả xe");
			listButton.add(saveButton);
			saveButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = listButton.indexOf(saveButton);
					bikeBackController.bikeBackIndex(index);
					ABikeBackDataListDialog.this.dispose();
				}
			});
			c.gridx = 1;
			c.gridy = getLastRowIndex();
			getContentPane().add(saveButton, c);
		}

		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}

	public abstract void buildControls(int i);

	public abstract T getNewData(int i);

	public abstract String getParkingId(int i);

	public BikeBackController getBikeBackController() {
		return bikeBackController;
	}

	public void setBikeBackController(BikeBackController bikeBackController) {
		this.bikeBackController = bikeBackController;
	}
}
