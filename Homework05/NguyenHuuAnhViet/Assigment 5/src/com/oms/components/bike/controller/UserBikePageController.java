package com.oms.components.bike.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.bike.Bike;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.abstractdata.gui.ADataSearchPane;
import com.oms.components.abstractdata.gui.ADataSinglePane;
import com.oms.components.bike.gui.BikeSearchPane;
import com.oms.components.bike.gui.BikeSinglePane;
import com.oms.components.bike.gui.UserSearchBikeListPane;
import com.oms.serverapi.BikeApi;

public class UserBikePageController extends ADataPageController<Bike> {
	public UserBikePageController() {
		super();
	}
	
	@Override
	public ADataListPane<Bike> createListPane() {
		return new UserSearchBikeListPane(this);
	}

	@Override
	public ADataSearchPane createSearchPane() {
		return new BikeSearchPane();
	}

	@Override
	public List<? extends Bike> search(Map<String, String> searchParams) {
		return BikeApi.singleton().getBikes(searchParams, BikeApi.PATH_NORMALBIKE);
	}

	@Override
	public ADataSinglePane<Bike> createSinglePane() {
		return new BikeSinglePane();
	}
}
