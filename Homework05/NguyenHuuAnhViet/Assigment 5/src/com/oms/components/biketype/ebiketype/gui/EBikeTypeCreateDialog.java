package com.oms.components.biketype.ebiketype.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.BikeType;
import com.oms.bean.EBikeType;
import com.oms.components.abstractdata.controller.IDataCreateController;
import com.oms.components.abstractdata.controller.IDataManageController;
import com.oms.components.biketype.gui.BikeTypeCreateDialog;

@SuppressWarnings("serial")
public class EBikeTypeCreateDialog extends BikeTypeCreateDialog{
	
	private JTextField soGioSacField;
	
	public EBikeTypeCreateDialog(BikeType biketype, IDataManageController<BikeType> controller) {
		super(biketype, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		if (t instanceof EBikeType) {
			EBikeType ebiketype = (EBikeType) t;
			
			int row = getLastRowIndex();
			JLabel nameLabel = new JLabel("So Gio Sac");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(nameLabel, c);
			soGioSacField= new JTextField(15);
			soGioSacField.setText(Float.toString(ebiketype.getSo_gio_sac()));
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(soGioSacField, c);
		}
	}
	
	@Override
	public BikeType getNewData() {
		super.getNewData();
		if (t instanceof EBikeType) {
			EBikeType ebiketype = (EBikeType) t;
			
			String w = soGioSacField.getText().trim();
			if (!w.equals("")) ebiketype.setSo_gio_sac(Float.parseFloat(soGioSacField.getText())); 
			else ebiketype.setSo_gio_sac((float)0);

		}
		
		return t;
	}
}