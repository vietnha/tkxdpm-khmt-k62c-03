package com.oms.serverapi;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.oms.app.user.OMSUser;
import com.oms.bean.Card;


public class AddCardBankApi {
	public static final String PATH = "http://localhost:8088/";
	
	private Client client;
	
	public AddCardBankApi() {
		client = ClientBuilder.newClient();
	}

	public Card match(Card card) {
		WebTarget webTarget = client.target(PATH).path("cards").path("match");
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(card, MediaType.APPLICATION_JSON));
		Card res = response.readEntity(Card.class);
		return res;
	}
	
}
