package com.oms.serverapi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.oms.bean.EBikeType;
import com.oms.bean.BikeType;
import com.oms.serverapi.BikeTypeApi;


public class EBikeTypeApiTest {
	private BikeTypeApi api = new BikeTypeApi();
	@Test
	public void testGetAllBikeType() {
		ArrayList<BikeType> list= api.getAllBikeTypes();
		assertEquals("Error in getAllBikeType API!", list.size(), 3);
	}
	@Test
	public void testGetAllEBikeType() {
		ArrayList<EBikeType> list= api.getEBikeTypes(null);
		assertEquals("Error in getAllEBikeType API!", list.size(), 1);
	}
	
	@Test(timeout = 1000)
	public void testResponse() {
		api.getAllBikeTypes();
	}
	
	@Test
	public void testUpdateEBikeType() {
		ArrayList<EBikeType> list= api.getEBikeTypes(null);
		assertTrue("No data", list.size() > 0);
		
		
		EBikeType ebiketype= list.get(0);
		String newName= "xe dap don dien 2";
		ebiketype.setName(newName);
		api.updateEBikeType(ebiketype);
		
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", newName);
		list = api.getEBikeTypes(params);
		assertTrue("Eror in updateEBikeType API!", list.size() > 0);
		
	
		EBikeType newEBikeType = api.getEBikeTypes(params).get(0);
		assertEquals("Eror in updateEBikeType API!", newEBikeType.getName(), newName);
	}

}
