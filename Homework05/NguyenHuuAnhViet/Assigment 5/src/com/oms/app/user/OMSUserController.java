package com.oms.app.user;

import javax.swing.JPanel;

import com.oms.bean.Card;
import com.oms.bean.RentalData;
import com.oms.bean.bike.Bike;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.bike.controller.UserBikePageController;
import com.oms.components.bike.ebike.UserEBikePageController;
import com.oms.components.bikeBack.controller.UserBikeBackPageController;
import com.oms.components.card.controller.UserCardPageController;
import com.oms.components.rental.controller.UserRentalPageController;



public class OMSUserController {
	public OMSUserController() {
	}
	
	public JPanel getCardPage() {
		ADataPageController<Card> controller = new UserCardPageController();
		return controller.getDataPagePane();
	}

	public JPanel getRentalPage() {
		ADataPageController<RentalData> controller = new UserRentalPageController();
		return controller.getDataPagePane();
	}

	public JPanel getBikeBackPage() {
		UserBikeBackPageController controller = new UserBikeBackPageController();
		return controller.getDataPagePane();
	}

	public JPanel getBikePage() {
		ADataPageController<Bike> controller = new  UserBikePageController();
		return controller.getDataPagePane();
	}

	public JPanel getEBikePage() {
		ADataPageController<Bike> controller = new UserEBikePageController();
		return controller.getDataPagePane();
	}
}