package com.oms.bean.bike;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public abstract class Bike {
	protected String id;
	protected String typeId;
	protected String parkingId;
	protected String name;
	protected float weight;
	protected String licensePlate;
	protected String manuafacturingDate;
	protected float cost;
	protected String producer;
	protected String nameType;
	protected int so_yen;
//	protected int so_ban_dap;
	protected int so_ghe_sau;
	protected int deposit;
	
	public Bike() {
		super();
	}
	public Bike(
		String id,
		String typeId,
		String parkingId,
		String name,
		float weight,
		String licensePlate,
		String manuafacturingDate,
		float cost,
		String producer,
		String nameType,
		int so_yen,
		int so_ban_dap,
		int so_ghe_sau,
		int deposit
	){
		this.id = id;
		this.typeId = typeId;
		this.parkingId = parkingId;
		this.name = name;
		this.weight = weight;
		this.cost = cost;
		this.producer = producer;
		this.manuafacturingDate = manuafacturingDate;
		this.licensePlate = licensePlate;
		this.nameType = nameType;
//		this.so_ban_dap = so_ban_dap;
		this.so_yen = so_yen;
		this.so_ghe_sau = so_ghe_sau;
		this.deposit = deposit;
	}

	public void cloneFromOtherBike(Bike bike) {
		if (bike instanceof Bike) {
			id = bike.id;
			typeId = bike.typeId;
			parkingId = bike.parkingId;
			name = bike.name;
			weight = bike.weight;
			cost = bike.cost;
			producer = bike.producer;
			manuafacturingDate = bike.manuafacturingDate;
			licensePlate = bike.licensePlate;
			nameType = bike.nameType;
//			so_ban_dap = bike.so_ban_dap;
			so_yen = bike.so_yen;
			so_ghe_sau = bike.so_ghe_sau;
			deposit = bike.deposit;
		}
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getParkingId() {
		return parkingId;
	}
	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getManuafacturingDate() {
		return manuafacturingDate;
	}
	public void setManuafacturingDate(String manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	public String getNameType() {
		return nameType;
	}
	public void setNameType(String nameType) {
		this.nameType = nameType;
	}
	public int getSo_yen() {
		return so_yen;
	}
	public void setSo_yen(int so_yen) {
		this.so_yen = so_yen;
	}
//	public int getSo_ban_dap() {
//		return so_ban_dap;
//	}
//	public void setSo_ban_dap(int so_ban_dap) {
//		this.so_ban_dap = so_ban_dap;
//	}
	public int getSo_ghe_sau() {
		return so_ghe_sau;
	}
	public void setSo_ghe_sau(int so_ghe_sau) {
		this.so_ghe_sau = so_ghe_sau;
	}
	public int getDeposit() {
		return deposit;
	}
	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	
	@Override
	public String toString() {
		return
			"id: " + this.id +
			"typeId: " + this.typeId +
			"parkingId: " + this.parkingId +
			"name: " + this.name +
			"weight: " + this.weight +
			"licensePlate: " + this.licensePlate +
			"manuafacturingDate: " + this.manuafacturingDate +
			"cost: " + this.cost +
			"producer: " + this.producer;
	}
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		
		if (bike.id != null && !bike.id.equals("") && !this.id.contains(bike.id)) {
			return false;
		}
		if (bike.typeId != null && !bike.typeId.equals("") && !this.typeId.contains(bike.typeId)) {
			return false;
		}
		if (bike.parkingId != null && !bike.parkingId.equals("") && !this.parkingId.contains(bike.parkingId)) {
			return false;
		}
		if (bike.name != null && !bike.name.equals("") && !this.name.contains(bike.name)) {
			return false;
		}
		if (bike.producer != null && !bike.producer.equals("") && !this.producer.contains(bike.producer)) {
			return false;
		}
		if (bike.manuafacturingDate != null && !bike.manuafacturingDate.equals("") && !this.manuafacturingDate.contains(bike.manuafacturingDate)) {
			return false;
		}
		if (bike.licensePlate != null && !bike.licensePlate.equals("") && !this.licensePlate.contains(bike.licensePlate)) {
			return false;
		}
		if (bike.cost != 0 && !(this.cost == bike.cost)) {
			return false;
		}
		if (bike.weight != 0 && !(this.weight == bike.weight)) {
			return false;
		}
		
		return true;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bike) {
			return this.id.equals(((Bike) obj).id);
		}
		return false;
	}
	
	public boolean checkBikeById(String id) {
		return this.id.equals(id);
	}
	
	public boolean checkBikeByParkingId(String parkingId) {
		return this.parkingId.equals(parkingId);
	}
}