# Các packages (classes) đã làm :
#### Server:
- com.oms.bean.bike
- com.oms.config
- com.oms.db.bike
- com.oms.db.helper
- com.oms.db.seed.bike
- com.oms.payload.bike
- com.oms.service.bike
- com.oms.util
- com.oms.util.log
#### Client:
- com.oms.bean.bike
- com.oms.components.abstract.IDataCreateController
- com.oms.components.abstract.ADataCreateDialog
- com.oms.components.bike
- com.oms.components.bike.controller
- com.oms.components.bike.gui
- com.oms.components.bike.ebike
- com.oms.serverapi.BikeApi
- com.oms.util
- com.oms.util.log
# Các packages (classes) có chỉnh sửa:
- com.oms.app.admin
- com.oms.app.user
# Test BlackBox BikeApi::getInstance().updateBike(), đặt tại:
- com.oms.serverapi.test.bike
# Test WhiteBox đặt tại: