package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.EBikeType;
import com.oms.bean.BikeType;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;

@Path("/ebiketype")
public class EBikeTypeService {

	private IMediaDatabase biketypeDatabase;

	public EBikeTypeService() {
		biketypeDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<BikeType> getEBikeTypes(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("so_yen") int so_yen, @QueryParam("tien_coc") int tien_coc,
			@QueryParam("so_ghe_sau") int so_ghe_sau, @QueryParam("muc_gia") float muc_gia, @QueryParam("so_gio_sac") float so_gio_sac
		) {
		EBikeType ebiketype = new EBikeType(id, name, so_yen, so_ghe_sau, muc_gia, tien_coc);
		ebiketype.setMuc_gia(muc_gia);
		ArrayList<BikeType> res = biketypeDatabase.searchBikeType(ebiketype);
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikeType updateEBikeType(@PathParam("id") String id, EBikeType ebiketype) {
		return biketypeDatabase.updateBikeType(ebiketype);
	}
}