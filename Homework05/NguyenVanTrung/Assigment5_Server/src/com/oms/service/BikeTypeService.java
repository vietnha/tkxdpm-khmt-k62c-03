package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.BikeType;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;

@Path("/biketypes")
public class BikeTypeService {
	
	private IMediaDatabase biketypeDatabase;
	
	public BikeTypeService() {
		biketypeDatabase = JsonMediaDatabase.singleton();
	}

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<BikeType> getAllBikeTypes() {
    	ArrayList<BikeType> res = biketypeDatabase.searchBikeType(null);
        return res;
    }
}