package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.NormalBikeType;
import com.oms.bean.BikeType;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;

@Path("/normalbiketype")
public class NormalBikeTypeService {

	private IMediaDatabase biketypeDatabase;

	public NormalBikeTypeService() {
		biketypeDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<BikeType> getNormalBikes(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("so_yen") int so_yen, @QueryParam("tien_coc") int tien_coc, 
			@QueryParam("so_ghe_sau") int so_ghe_sau, @QueryParam("muc_gia") float muc_gia
		) {
		NormalBikeType normalbiketype = new NormalBikeType(id, name, so_yen, so_ghe_sau, muc_gia, tien_coc);
		ArrayList<BikeType> res = biketypeDatabase.searchBikeType(normalbiketype);
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikeType updateNormalBikeType(@PathParam("id") String id, NormalBikeType normalbiketype) {
		return biketypeDatabase.updateBikeType(normalbiketype);
	}
}