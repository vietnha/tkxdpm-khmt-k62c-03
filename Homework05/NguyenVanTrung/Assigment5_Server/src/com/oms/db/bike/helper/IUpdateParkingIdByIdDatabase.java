package com.oms.db.bike.helper;

import com.oms.bean.bike.Bike;

public interface IUpdateParkingIdByIdDatabase {
	public Bike updatParkingIdById(String id, String parkingId);
}
