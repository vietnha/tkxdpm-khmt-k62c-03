package com.oms.db.bike;

import com.oms.util.log.IClassLoggerable;

public interface IBikeInListDatabase extends IClassLoggerable, IAddBikeDatabase, IRemoveBikeDatabase, ISearchBikeDatabase, IUpdateBikeDatabase {}