package com.oms.util.log;

public interface IClassLoggerable {
	public void writeLog(String description);
	public void writeLogToJsonFile(String nameFile);
}
