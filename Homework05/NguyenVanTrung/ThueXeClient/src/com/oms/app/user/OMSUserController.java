package com.oms.app.user;

import javax.swing.JPanel;

import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.rental.controller.UserRentalPageController;



public class OMSUserController {
	public OMSUserController() {
	}
	
	public JPanel getRentalPage() {
		ADataPageController<RentalData> controller = new UserRentalPageController();
		return controller.getDataPagePane();
	}
}