package com.oms.components.rental.gui;

import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.oms.bean.RentalData;
import com.oms.bean.Card;
import com.oms.components.abstractdata.controller.RentalController;
import com.oms.components.abstractdata.gui.ARentalDataListDialog;
import com.oms.serverapi.RentalApi;
import com.oms.serverapi.CardBankApi;

@SuppressWarnings("serial")
public class CardListDialog extends ARentalDataListDialog<Card>{

	public CardListDialog(List<Card> card, RentalData rentalData) {
		super(card, rentalData);
	}

	@Override
	public void buildControls(int i) {
		int row = getLastRowIndex();
		JLabel idLabel = new JLabel("Card ID: " + t.get(i).getId());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(idLabel, c);
		
		row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name: " + t.get(i).getName());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		
		row = getLastRowIndex();
		JLabel moneyLabel = new JLabel("Money: " + t.get(i).getMoney());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(moneyLabel, c);
		
		row = getLastRowIndex();
		JLabel securityCodeLabel = new JLabel("Activate date: " + t.get(i).getSecurityCode());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(securityCodeLabel, c);
		
		row = getLastRowIndex();
		JLabel exprirationDateLabel = new JLabel("Expriration date: " + t.get(i).getExprirationDate());
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(exprirationDateLabel, c);
	}

	@Override
	public Card getNewData(int i) {
		return t.get(i);
	}

	@Override
	public String getCardId(int i) {
		return t.get(i).getId();
	}

}

