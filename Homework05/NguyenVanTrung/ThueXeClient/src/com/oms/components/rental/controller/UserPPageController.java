package com.oms.components.rental.controller;




import com.oms.bean.RentalData;
import com.oms.components.abstractdata.controller.ADataPageController;
import com.oms.components.abstractdata.gui.ADataListPane;
import com.oms.components.rental.gui.UserBikeListPane;


public abstract class UserPPageController extends ADataPageController<RentalData> {
	public UserPPageController() {
		super();
	}
	
	@Override
	public ADataListPane<RentalData> createListPane() {
		return new UserBikeListPane(this);
	}
	
}
