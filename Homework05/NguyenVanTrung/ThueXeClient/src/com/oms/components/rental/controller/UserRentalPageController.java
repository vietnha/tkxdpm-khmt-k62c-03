package com.oms.components.rental.controller;

import java.util.List;
import java.util.Map;

import com.oms.bean.RentalData;
import com.oms.components.rental.gui.BikeSearchPane;
import com.oms.components.rental.gui.BikeSingelPane;
import com.oms.serverapi.RentalApi;



public class UserRentalPageController extends UserPPageController{
	@Override
	public List<? extends RentalData> search(Map<String, String> searchParams) {
		return new RentalApi().getBike(searchParams);
	}
	
	@Override
	public BikeSingelPane createSinglePane() {
		return new BikeSingelPane();
	}
	
	@Override
	public BikeSearchPane createSearchPane() {
		return new BikeSearchPane();
	}

	
	
}
