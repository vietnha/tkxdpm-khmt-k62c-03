
package com.oms.components.rental.gui;
import javax.swing.JLabel;

import com.oms.bean.RentalData;
import com.oms.components.abstractdata.gui.ADataSinglePane;


@SuppressWarnings("serial")
public class BikeSingelPane extends ADataSinglePane<RentalData>{
	private JLabel labelName;
	private JLabel labelAddress;
	private JLabel labelDistance;
	private JLabel labelTime;
	private JLabel labelNumberOfBikes;
	private JLabel labelNumberOfeBikes;
	private JLabel labelNumberOfTwinBikes;
	private JLabel labelNumberOfEmtyDocks;
	
	public BikeSingelPane() {
		super();
	}
		
	
	public BikeSingelPane(RentalData bike) {
		this();
		this.t = bike;
		
		displayData();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
	int row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelName = new JLabel();
	add(labelName, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelAddress = new JLabel();
	add(labelAddress, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelDistance = new JLabel();
	add(labelDistance, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelTime = new JLabel();
	add(labelTime, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfBikes = new JLabel();
	add(labelNumberOfBikes, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfeBikes = new JLabel();
	add(labelNumberOfeBikes, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfTwinBikes = new JLabel();
	add(labelNumberOfTwinBikes, c);
	
	row = getLastRowIndex();
	c.gridx = 0;
	c.gridy = row;
	labelNumberOfEmtyDocks = new JLabel();
	add(labelNumberOfEmtyDocks, c);

	}
	
	
	
	
	@Override
	public void displayData() {
		labelName.setText("Name of bike: " + t.getName());
		labelAddress.setText("Bike name: " + t.getNameType());
		labelDistance.setText("ParkingId: " + t.getParkingId());
		labelNumberOfBikes.setText("Weight of bike: " + t.getWeight());
		labelNumberOfeBikes.setText("Cost of bike : " + t.getCost());
		labelNumberOfTwinBikes.setText("Số yên/ bàn đạp : " + t.getSo_yen());
		labelNumberOfEmtyDocks.setText("Giá đặt cọc: " + t.getTien_coc());
		labelTime.setText("Số ghế sau: " + t.getSo_ghe_sau());
		
	}
}
