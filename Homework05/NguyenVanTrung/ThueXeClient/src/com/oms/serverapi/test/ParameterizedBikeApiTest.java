package com.oms.serverapi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.oms.bean.RentalData;
import com.oms.serverapi.RentalApi;

@RunWith(Parameterized.class)

public class ParameterizedBikeApiTest {
	private String bikeName;
	private String expectedResult;
	
	private RentalApi api = new RentalApi();
	
	
	public ParameterizedBikeApiTest(String bikeName, String expectedResult) {
		super();
		this.bikeName = bikeName;
		this.expectedResult = expectedResult;
	}
	

	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { 
			{ "A", "A bãi đỗ xe tạ quang bửu" },
			{ "B", "B bãi đỗ xe bách khoa" }, 
			
			{ "C", "C bãi đỗ xe hoàng hà" },
			{ "D", "D bãi đỗ xe xuân hòa" }
		});
	}
	
	@Test
	public void testGetBike() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", bikeName);
		
		ArrayList<RentalData> list= api.getBike(params);
		assertTrue("No data", list.size() > 0);
		
		
		RentalData bike = list.get(0);
		assertEquals("Eror in getBooks API!", bike.getName(), expectedResult);
	}

}
