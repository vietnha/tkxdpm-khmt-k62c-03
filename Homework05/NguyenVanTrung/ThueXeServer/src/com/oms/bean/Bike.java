package com.oms.bean;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("bike")
@JsonSubTypes({ @Type(value = Bike.class, name = "bike") })
public class Bike {
	private String id;
	private String name;
	private String address;
	private int numberOfBikes;
	private int numberOfeBikes;
	private int numberOfTwinBikes;
	private int numberOfEmtyDocks;
	private float distance;
	private float time;
	
	public Bike() {
		super();
	}
	public Bike(String id,String name,String address,int numberOfBikes,
			int numberOfeBikes,int numberOfTwinBikes,int numberOfEmtyDocks,float distance,float time
			) {
		this.id = id;
		this.name=name;
		this.address=address;
		this.numberOfBikes=numberOfBikes;
		this.numberOfeBikes=numberOfeBikes;
		this.numberOfTwinBikes=numberOfTwinBikes;
		this.numberOfEmtyDocks=numberOfEmtyDocks;
		this.distance=distance;
		this.time=time;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public int getNumberOfBikes() {
		return numberOfBikes;
	}

	public void setNumberOfBikes(int numberOfBikes) {
		this.numberOfBikes = numberOfBikes;
	}
	
	public int getNumberOfTwinBikes() {
		return numberOfTwinBikes;
	}

	public void setNumberOfTwinBikes(int numberOfTwinBikes) {
		this.numberOfTwinBikes = numberOfTwinBikes;
	}
	public int getNumberOfEmtyDocks() {
		return numberOfEmtyDocks;
	}

	public void setNumberOfEmtyDocks(int numberOfEmtyDocks) {
		this.numberOfEmtyDocks = numberOfEmtyDocks;
	}
	public int getNumberOfeBikes() {
		return numberOfeBikes;
	}

	public void setNumberOfeBikes(int numberOfeBikes) {
		this.numberOfeBikes = numberOfeBikes;
	}
	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}
	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "id: " + this.id + ", name: " + this.name + 
				", address: " + this.address+ ", numberOfBikes: " + this.numberOfBikes+", numberOfeBikes: " + this.numberOfeBikes+", numberOfTwinBikes: " + this.numberOfTwinBikes
				+", numberOfEmtyDocks: " + this.numberOfEmtyDocks + ", distance: " + this.distance + ", time: " + this.time ;
	}
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		
		
		if (bike.id != null && !bike.id.equals("") && !this.id.contains(bike.id)) {
			return false;
		}
		if (bike.name != null && !bike.name.equals("") && !this.name.contains(bike.name)) {
			return false;
		}
		if (bike.address != null && !bike.address.equals("") && !this.address.contains(bike.address)) {
			return false;
		}
		if (bike.numberOfBikes != 0 && this.numberOfBikes != bike.numberOfBikes) {
			return false;
		}
		if (bike.numberOfeBikes != 0 && this.numberOfeBikes != bike.numberOfeBikes) {
			return false;
		}
		if (bike.numberOfEmtyDocks != 0 && this.numberOfEmtyDocks != bike.numberOfEmtyDocks) {
			return false;
		}
		if (bike.numberOfTwinBikes != 0 && this.numberOfTwinBikes != bike.numberOfTwinBikes) {
			return false;
		}
		if (bike.distance != 0 && this.distance != bike.distance) {
			return false;
		}
		if (bike.time != 0 && this.time != bike.time) {
			return false;
		}
		
		return true;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bike) {
			return this.id.equals(((Bike) obj).id);
		}
		return false;
	}
	
	public boolean matchId(Bike bike) {
		if (bike == null)
			return false;
		
		if (!this.id.equals(bike.id)) {
			return false;
		}
		
		return true;
	}
	
}