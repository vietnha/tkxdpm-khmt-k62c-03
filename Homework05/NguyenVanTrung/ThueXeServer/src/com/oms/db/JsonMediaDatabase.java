package com.oms.db;

import java.util.ArrayList;
import com.oms.bean.Bike;
import com.oms.db.seed.Seed;

public class JsonMediaDatabase implements IMediaDatabase{
	private static IMediaDatabase singleton = new JsonMediaDatabase();
	private ArrayList<Bike> bikes = Seed.singleton().getBike();
	
	private JsonMediaDatabase() {
	}
	
	public static IMediaDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Bike> searchBike(Bike bike) {
		ArrayList<Bike> res = new ArrayList<Bike>();
		for (Bike b: bikes) {
			if (b.match(bike)) {
				res.add(b);
			}
		}
		return res;
	}
	
	@Override
	public ArrayList<Bike> searchBikeById(Bike bike) {
		ArrayList<Bike> res = new ArrayList<Bike>();
		for (Bike b: bikes) {
			if (b.matchId(bike)) {
				res.add(b);
			}
		}
		return res;
	}
	
	

	@Override
	public Bike addBike(Bike bike) {
		for (Bike m: bikes) {
			if (m.equals(bike)) {
				return null;
			}
		}
		
		bikes.add(bike);
		return bike;
	}
	
	@Override
	public Bike updateBike(Bike bike) {
		for (Bike m: bikes) {
			if (m.equals(bike)) {
				bikes.remove(m);
				bikes.add(bike);
				return bike;
			}
		}
		return null;
	}
}
