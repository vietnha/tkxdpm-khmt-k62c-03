package com.oms.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oms.bean.Bike;

public class Seed {
	
	private ArrayList<Bike> bike;
	
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		bike = new ArrayList<Bike>();
		bike.addAll(generateDataFromFile( new File(getClass().getResource("./bike.json").getPath()).toString()));
		
	}
	
	private ArrayList<? extends Bike> generateDataFromFile(String filePath){
		ArrayList<? extends Bike> res = new ArrayList<Bike>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	public ArrayList<Bike> getBike() {
		return bike;
	}


	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}
}
