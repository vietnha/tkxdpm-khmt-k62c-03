package com.oms.db;

import java.util.ArrayList;

import com.oms.bean.Bike;

public interface IMediaDatabase {
	public ArrayList<Bike> searchBike(Bike bike);
	public Bike updateBike(Bike bike);
	public Bike addBike(Bike bike);
	public ArrayList<Bike> searchBikeById(Bike bike);

}
