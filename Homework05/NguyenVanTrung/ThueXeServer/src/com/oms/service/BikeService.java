package com.oms.service;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.oms.bean.Bike;
import com.oms.db.IMediaDatabase;
import com.oms.db.JsonMediaDatabase;

@Path("/bike")
public class BikeService {

	private IMediaDatabase mediaDatabase;

	public BikeService() {
		mediaDatabase = JsonMediaDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getBike(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("address") String address, @QueryParam("distance") float distance,
			@QueryParam("time") float time) {
		Bike bike = new Bike(id, name, address, 0, 0, 0, 0, distance, time);
		bike.setName(name);
		bike.setAddress(address);
		bike.setDistance(distance);
		bike.setTime(time);
		ArrayList<Bike> res = new ArrayList<>();
		if (mediaDatabase.searchBikeById(bike).size() > 0)
			res.add(mediaDatabase.searchBikeById(bike).get(0));
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Bike updateBike(@PathParam("id") String id, Bike bike) {
		return mediaDatabase.updateBike(bike);
	}
}