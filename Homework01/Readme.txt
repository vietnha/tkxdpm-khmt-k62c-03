Họp lần 1 ngày 19-10-2020, nhóm đọc yêu cầu đề bài và thảo luận vẽ biểu đồ use case tổng quan.
Mỗi thành viên tự lựa chọn usecase của mình, nhóm đã phân công như sau:

- Trả xe: Trần Đình Thắng
- Thuê xe: Nguyễn Văn Trung
- Quản lý thông tin thẻ: Nguyễn Thị Thu Thành
- Quản lý thông tin bãi xe: Mai Thị Thảo
- Quản lý thông tin xe: Nguyễn Hữu Anh Việt
- Quản lý loại xe: Bùi Phó Bền
- Quản lý giá cho thuê: Nguyễn Thị Duyên

Sau khi thống nhất biểu đồ usecase tổng quan và phân chia xong use case từng thành viên, 
mỗi thành viên tiến hành triển khai use case của mình và đặc tả use case chi tiết.
